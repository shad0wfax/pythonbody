import numpy as np
import re
import warnings

from .nbdf import nbdf
from .ffi import ffi

def calc_shell_data(data: nbdf,
                    n_shells: int = 100,
                    shells: list = None,
                    by: str = "M",
                    method: str = "mean",
                    std: bool = False
                   ):
    """
    Divides the given data into shells, and calculates the averages
    within these shells.

    Parameters:
        data (nbdf): data to divide into shells
        n_shells (int): number of shells to create
        shells (list): alternatively to ``n_shells`` give list of shells to consider
        by (str): column name to sort and divide into shells by.
        method (str): can be 'sum', 'mean', 'cumsum', 'cummean' defaults to mean
            sum: take sum of each value per shell
            mean: takes average of each value per shell
            cumsum: Sum of all values below the shell threshold
            cummean: Average of all values below the shell threshold
        std (bool): include standard deviation in calculations

    Returns:
        nbdf: Nbody Dataframe containing the shell data.
    """

    try:
        return ffi.calc_shells(data=data,
                        by=by,
                        n_shells=n_shells,
                        shells=shells,
                        method=method,
                        std=std
                        )
    except:
        warnings.warn("Couldn't use FFI. Using python way, which may be way slower!")
        return calc_shell_data_python(data=data,
                        by=by,
                        n_shells=n_shells,
                        shells=shells,
                        method=method,
                        std=std
                        )


def calc_shell_data_python(data: nbdf,
                    n_shells: int = 100,
                    shells: list = None,
                    by: str = "M",
                    method: str = "mean",
                    std: bool = False
                   ):
    list_shells = None

    if method not in ["sum", "mean", "cumsum", "cummean"]:
        raise ValueError(f"Method must be one of 'sum', 'mean', 'cumsum' or 'cummean', but is {method}")

    if by not in data.columns:
        try:
            eval(f"data.calc_{by}()")
        except:
            raise KeyError(f"Couldn't find or calculate Key '{by}' in the data!")

    if not re.search(".*/.*T", by) and f"{by}/{by}T" not in data.columns:
        try:
            eval(f"data.calc_{by}_over_{by}T()")
        except:
            raise KeyError(f"Couldn't find or calculate Key '{by}/{by}T' in the data!")
    
    by = f"{by}/{by}T"
    data = data.sort_values(by=by)
    
    if shells is None:
        by_min = data[by].min()
        by_max = data[by].max()
        by_step = (by_max - by_min)/n_shells
        shells = np.linspace(0,by_max,n_shells+1)/by_max
    else:
        if shells[0] != 0:
            shells = [0] + list(shells)
        n_shells = len(shells) - 1

    df = nbdf(columns=["N"] + list(data.columns))
    
    empty_count = 0
    for i in range(0, n_shells):
        N = np.sum((data[by] > shells[i]) & (data[by] <= shells[i+1]))
        if N == 0:
            empty_count += 1
            continue

        if method == "mean":
            df.loc[shells[i+1]] = data.loc[(data[by] > shells[i]) & (data[by] <= shells[i+1])].mean()
            df.loc[shells[i+1],"N"] = N
        if method == "sum":
            df.loc[shells[i+1]] = data.loc[(data[by] > shells[i]) & (data[by] <= shells[i+1])].sum()
            df.loc[shells[i+1],"N"] = N
        elif method == "cummean":
            df.loc[shells[i+1]] = data.loc[(data[by] <= shells[i+1])].mean()
            df.loc[shells[i+1],"N"] = N
        elif method == "cumsum":
            df.loc[shells[i+1]] = data.loc[(data[by] <= shells[i+1])].sum()
            df.loc[shells[i+1],"N"] = N

        if std:
            for col in data.columns:
                if method == "mean" or method == "sum":
                    df.loc[shells[i+1], col + "_std"] = data.loc[(data[by] > shells[i]) & (data[by] <= shells[i+1]), col].std()
                elif method == "cummean" or method == "cumsum":
                    df.loc[shells[i+1], col + "_std"] = data.loc[(data[by] <= shells[i+1]), col].std()
        
        df.loc[shells[i+1], by] = shells[i+1]

    return df

