import pandas as pd
import numpy as np

from pythonbody.ffi import ffi

class nbdf(pd.DataFrame):
    """
    Basic DataFrame used most of the time. Extends pandas.DataFrame, adds a lot
    of calculatable values from the raw data.

    This is the base class for handling an entire cluster with all it's data

    :param \*args: see pandas.DataFrame documentation
    :param \*\*kwargs: see pandas.DataFrame documentation
    """
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self._shell_data = None

    @property
    def shell_data(self):
        if self._shell_data is None:
            self._shell_data = self.get_shell_data()
        return self._shell_data


    def __getitem__(self, value):
        """
        checks if passed value(s) are in currently loaded dataframe, otherwise returns snap list data
        """
        ret = None
        try:
            ret = super().__getitem__(value)
        except:
            ret = None
        if ret is None:
            try:
                ret = super().loc[value.values[:,0]]
            except:
                ret = None
        if ret is None:
            try:
                ret = super().loc[:,value]
            except:
                ret = None
        
        # Check if item is calulatable
        if ret is None:
            if type(value) != list:
                value = [value]

            missing_list = []
            for val in value:
                if val not in self.columns:
                    missing_list += [val]
                
            if len(missing_list) == 0:
                ret = super().__getitem__(value)
            elif len(missing_list) > 0 and np.sum([f"calc_{val}".replace("/","_over_") not in dir(self) for val in missing_list]) == 0:
                for missing in missing_list:
                    if missing not in self.columns:
                        eval(f"self.calc_{missing}()".replace("/","_over_"))
                ret = self[value[0] if len(value) == 1 else value]
            else:
                raise KeyError(f"Couldn't get key(s) {missing_list}")

        if type(ret) == pd.core.frame.DataFrame:
            return self.pd_df_to_class(ret)
        return ret
    
    def pd_df_to_class(self, ret):
        """
        Changes pandas DataFrame to instalce of nbody_data_frame
        """
        return nbdf(ret)

    @property
    def COM(self):
        """
        return: center of mass
        rtype: float[3]
        """
        return 1/self.loc[:, "M"].sum() * np.sum(self.loc[:, ["X1", "X2", "X3"]].multiply(self.loc[:, "M"], axis=0))

    def adjust_COM(self, by="M", N=None):
        """
        corrects positions such that COM is at (0,0,0)
        """
        if N is None:
            N = self.shape[0]
        elif type(N) == str and N[-1] == "%":
            N = int(float(N[:-1])/100 * self.shape[0])
        if by=="M":
            self.loc[:,["X1", "X2", "X3"]] = self.loc[:,["X1", "X2", "X3"]] - self.COM
        else:
            N_idx = self.iloc[:N].index[-1]
            COM = 1/self.loc[:N_idx, by].sum() * np.sum(self.loc[:N_idx, ["X1", "X2", "X3"]].multiply(self.loc[:N_idx, by], axis=0))
            #print(f"Adjusting COM with N = {N}, COM={list(COM)}")
            self.loc[:,["X1", "X2", "X3"]] = self.loc[:,["X1", "X2", "X3"]] - COM



    @property
    def Rh(self):
        """
        gets the half mass radius
        return: half mass radius
        rtype: float
        """
        return self.radius_from_mass_fraction(mass_fraction=0.5)

    
    def __repr__(self):
        return super().__repr__()
    
    def _repr_html_(self):
        return super()._repr_html_()

    def calc(self, *args):
        if len(args) != 0:
            for arg in args:
                if f"calc_{arg}" in dir(self):
                    eval(f"self.calc_{arg}()".replace("/","_over_"))
                else:
                    raise KeyError(f"Couldn't calculate {arg}")
        else:
            methods = dir(self)
            for method in methods:
                if "calc_" in method:
                    eval(f"self.calc_{method}()".replace("/","_over_"))

    def calc_all(self):
        for func in [func for func in dir(self) if "calc_" in func and func not in ["calc_R", "calc_THETA", "calc_PHI", "calc_all"]]:
            eval(f"self.{func}()")

    def calc_spherical_coords(self):
        """
        calculates spherical coordinates from cartesian ones.
        See https://en.wikipedia.org/wiki/Spherical_coordinate_system#Cartesian_coordinates

        | Required columns: ``X1``, ``X2``, ``X3``        
        | Output columns: ``R``, ``THETA``, ``PHI``
        """
        x = self["X1"].values
        y = self["X2"].values
        z = self["X3"].values

        """vx = self["V1"]
        vy = self["V2"]
        vz = self["V3"]"""

        self["R"] = r = np.sqrt(x**2 + y**2 + z**2)
        self["THETA"] = np.arccos(z/r)
        self["PHI"] = np.arctan2(y, x)

        """self["SPHCO_V_R"] = vr = np.sqrt(vx**2 + vy**2 + vz**2)
        self["SPHCO_V_THETA"] = np.arccos(vz/vr)
        self["SPHCO_V_PHI"] = np.arctan2(vy, vz)

        self["V_SPHCO_R"] = (x*vx+y*vy+z*vz)/r

        h1 = vz/r
        h2 = (z*(2*x*vx+2*y*vy+2*z*vz))/(2*(r)**3)
        h3 = np.sqrt(1 - (z**2/r**2))

        self["V_SPHCO_THETA"] = - ((h1 - h2) / h3)
        self["V_SPHCO_PHI"] = (y*vx-x*vy)/(x**2+y**2)"""
       
        """mask = self["X1"] > 0
        self.loc[mask,"PHI"] = np.arctan(self.loc[mask,"X2"]/self.loc[mask,"X1"])
        mask = (self["X1"] < 0) & (self["X2"] >= 0)
        self.loc[mask,"PHI"] = np.arctan(self.loc[mask,"X2"]/self.loc[mask,"X1"]) + np.pi
        mask = (self["X1"] < 0) & (self["X2"] < 0)
        self.loc[mask,"PHI"] = np.arctan(self.loc[mask,"X2"]/self.loc[mask,"X1"]) - np.pi
        mask = (self["X1"] == 0) & (self["X2"] > 0)
        self.loc[mask,"PHI"] = np.pi/2
        mask = (self["X1"] == 0) & (self["X2"] < 0)
        self.loc[mask,"PHI"] = -np.pi/2
        self["R/RT"] = self["R"]/self["R"].max()"""

        self.sort_values("R",ignore_index=True,inplace=True)

    def calc_R(self):
        """
        maps to calc_spherical_coords
        """
        return self.calc_spherical_coords()

    def calc_THETA(self):
        """
        maps to calc_spherical_coords
        """
        return self.calc_spherical_coords()

    def calc_PHI(self):
        """
        maps to calc_spherical_coords
        """
        return self.calc_spherical_coords()

    def calc_cylindrical_coords(self):
        """
        calculates cylindrical coordinates from cartesian coords.
        See https://en.wikipedia.org/wiki/Cylindrical_coordinate_system

        | Required columns: ``X1``, ``X2``, ``X3``
        | Optional columns: ``V1``, ``V2``, ``V3``
        | Output columns: ``CYLCO_RHO``, ``CYLCO_PHI``, ``CYLCO_Z``
        """
        x = self["X1"]
        y = self["X2"]
        z = self["X3"]
        vx = self["V1"]
        vy = self["V2"]
        vz = self["V3"]

        self["CYLCO_RHO"] = rho = np.sqrt(x**2 + y**2)
        self["CYLCO_PHI"] = np.arctan2(y,x)        
        self["CYLCO_Z"] = z

        h1 = vx/rho
        h2 = (x*(2*x*vx+2*y*vy))/(2*rho**3)
        h3 = np.sqrt(1-(x**2/(x**2+y**2)))

        self["V_CYLCO_RHO"] = (self["X1"]*self["V1"]+self["X2"]*self["V2"])/rho
        self["V_CYLCO_PHI"] = (y*vx-x*vy)/(x**2+y**2)
        self["V_CYLCO_PHI2"] = np.sign(y)*((h1-h2/h3))
        self["V_CYLCO_Z"] = self["V3"]


        self["CYLCO_V_RHO"] = np.sqrt(vx**2+vy**2)        
        self["CYLCO_V_PHI"] = np.arctan2(vy,vx)
        self["CYLCO_V_Z"] = vz
        """mask = (vx == 0) & (y != 0)
        self.loc[mask, "CYLCO_V_PHI"] = np.pi/2 * vy[mask]/np.abs(vy[mask]) 
        mask = x >= 0
        self.loc[mask, "CYLCO_V_PHI"] = np.arctan(vy[mask]/vx[mask])
        mask = (x < 0) & (y >= 0)
        self.loc[mask, "CYLCO_V_PHI"] = np.arctan(vy[mask]/vx[mask]) + np.pi
        mask = (x < 0) & (y < 0)
        self.loc[mask, "CYLCO_V_PHI"] = np.arctan(vy[mask]/vx[mask]) - np.pi
        #self["CYLCO_V_PHI"] = np"""

    def calc_EKIN(self):
        """
        calculates kinetic energy into ``EKIN`` column. 

        | Required columns: ``M``, ``V1``, ``V2``, ``V3``        
        | Output columns: ``EKIN``
        """
        self["EKIN"] = 0.5*self["M"]*np.linalg.norm(self[["V1", "V2", "V3"]], axis=1)**2

    def calc_EKIN_spec(self):
        """
        calculates specific kinetic energy into ``EKIN_spec`` column. 

        | Required columns: ``V1``, ``V2``, ``V3``        
        | Output columns: ``EKIN_spec``
        """
        self["EKIN_spec"] = 0.5*np.linalg.norm(self[["V1", "V2", "V3"]], axis=1)**2

    def calc_EROT_spec(self, kwargs_calc_vrot: dict = {}):
        """
        calculates specific rotational energy from VROT

        | Required columns: ``X1``, ``X2``, ``X3``, ``V1``, ``V2``, ``V3``
        | Intermediary columns: ``VROT``
        | Output columns: ``EROT_spec``

        :param kwargs_calc_vrot: parameters for calc_VROT()
            e.g. { "method": "nbody", "sign_nbody": True}
        :type kwargs_calc_vrot: dict
        """
        if "VROT" not in self.columns:
            self.calc_VROT(**kwargs_calc_vrot)
        
        self["EROT_spec"] = 0.5*self.loc[:, "VROT"]**2
    
    def calc_EROT(self, kwargs_calc_vrot: dict = {}):
        """
        calculates rotational energy from VROT

        | Required columns: ``M``, ``X1``, ``X2``, ``X3``, ``V1``, ``V2``, ``V3``
        | Intermediary columns: ``VROT``
        | Output columns: ``EROT``

        :param kwargs_calc_vrot: parameters for calc_VROT()
            e.g. { "method": "nbody", "sign_nbody": True}
        :type kwargs_calc_vrot: dict
        """
        if "VROT" not in self.columns:
            self.calc_VROT(**kwargs_calc_vrot)
        
        self["EROT"] = 0.5*self.loc[:, "VROT"]**2 * self.loc[:, "M"]

    def calc_EPOT(self, G: float = 1):
        """
        calculates potential energy.

        :param G: gravitational constant
        :type G: float

        | Required columns: ``M``, ``X1``, ``X2``, ``X3``
        | Output columns: ``EPOT``
        """
        self["EPOT"] = G * ffi.grav_pot(self.loc[:, ["M", "X1", "X2", "X3"]])
    
    def calc_EPOT_spec(self, G: float = 1):
        """
        calculates specific potential energy.

        :param G: gravitational constant
        :type G: float

        | Required columns: ``M``, ``X1``, ``X2``, ``X3``
        | Intermediary columns: ``EPOT``
        | Output columns: ``EPOT_spec``
        """
        if "EPOT" not in self.columns:
            self.calc_EPOT(G)

        self["EPOT_spec"] = self.loc[:, "EPOT"].values \
                                / self.loc[:, "M"].values

    def calc_Eb_spec(self):
        """
        calculate specific binding energy (E_b = E_kin + E_pot).

        | Required columns: ``V1``, ``V2``, ``V3``, ``POT``
        | Intermediary columns: ``EKIN_spec``
        | Output columns ``Eb_spec``
        """
        if "EKIN_spec" not in self.columns:
            self.calc_EKIN_spec()
        self["Eb_spec"] = self["EKIN_spec"] + self["POT"]

    def calc_Eb(self):
        """
        calculate binding energy (E_b = E_kin + E_pot), and uses Eb_spec
        as intermediate result.

        | Required columns: ``V1``, ``V2``, ``V3``, ``POT``
        | Intermediary columns: ``Eb_spec``, ``EKIN_spec``
        | Output columns: ``Eb``
        """

        if "Eb_spec" not in self.columns:
            self.calc_Eb_spec()
        self["Eb"] = self["Eb_spec"] * self["M"]

    def calc_ETOT_spec(self):
        """
        calculate specific total energy (E_tot = E_kin + E_pot) of each particle

        | Required columns: ``V1``, ``V2``, ``V3``, ``POT``
        | Output columns: ``ETOT_spec``
        """
        self["ETOT_spec"] = self["POT"] + 0.5 * np.linalg.norm(self[["V1", "V2", "V3"]],axis=1)**2
    
    def calc_ETOT(self):
        """
        calculate total energy (E_tot = E_kin + E_pot) of each particle

        | Required columns: ``M``, ``V1``, ``V2``, ``V3``, ``POT``
        | Intermediary columns: ``ETOT_spec``
        | Output columns: ``ETOT``
        """
        if "ETOT_spec" not in self.columns:
            self.calc_ETOT_spec()
        self["ETOT"] = self["ETOT_spec"] * self["M"]
    
    def calc_ETOT_over_ETOTT_spec(self):
        """
        calculate total energy (E_tot = E_kin + E_pot) of each particle over the
        total system energy

        | Required columns: (``V1``, ``V2``, ``V3``, ``POT``) | ('ETOT_spec')
        | Output columns: ``ETOT/ETOTT``
        """
        if "ETOT_spec" not in self.columns:
            self.calc_ETOT_spec()
        self["ETOT/ETOTT_spec"] = self["ETOT_spec"] / self["ETOT_spec"].sum()
    
    def calc_ETOT_over_ETOTT(self):
        """
        calculate specific total energy (E_tot = E_kin + E_pot) of each
        particle over the total system specific energy.

        | Required columns: (``M``, ``V1``, ``V2``, ``V3``, ``POT``) | ('ETOT')
        | Intermediary columns: 'ETOT_spec'
        | Output columns: ``ETOT/ETOTT``
        """
        if "ETOT/ETOTT_spec" not in self.columns:
            self.calc_ETOT_over_ETOTT_spec()
        self["ETOT/ETOTT"] = self["ETOT/ETOTT_spec"] * self["M"]

    def calc_ETOT_over_ETOTT_spec_cum(self):
        """
        calculate cumulative total specific energy (E_tot = E_kin + E_pot) of each particle over the
        total system energy

        | Required columns: (``V1``, ``V2``, ``V3``, ``POT``) | ('ETOT')
        | Output columns: ``ETOT/ETOTT``
        """
        if "ETOT/ETOTT" not in self.columns:
            self.calc_ETOT_over_ETOTT_spec()
        self.sort_values("ETOT/ETOTT_spec", inplace=True)
        self["ETOT/ETOTT_spec_cum"] = self["ETOT/ETOTT_spec"].cumsum()
    
    def calc_ETOT_over_ETOTT_cum(self):
        """
        calculate cumulative total energy (E_tot = E_kin + E_pot) of each particle over the
        total system energy

        | Required columns: (``M``, ``V1``, ``V2``, ``V3``, ``POT``) | ('ETOT/ETOTT')
        | Output columns: ``ETOT/ETOTT``
        """
        if "ETOT/ETOTT" not in self.columns:
            self.calc_ETOT_over_ETOTT()
        self.sort_values("ETOT/ETOTT", inplace=True)
        self["ETOT/ETOTT_cum"] = self["ETOT/ETOTT"].cumsum()


    def calc_LZ_spec(self):
        """
        calculates specific angular Momentum in z-Direction

        | Required columns: ``X1``, ``X2``, ``V1``, ``V2``
        | Output columns: ``LZ_spec``
        """
        X1 = self["X1"]
        X2 = self["X2"]
        V1 = self["V1"]
        V2 = self["V2"]

        if not np.allclose(self.COM, (0, 0, 0)):
            X1 = X1 - self.COM.values[0]
            X2 = X2 - self.COM.values[1]

        self["LZ_spec"] = X1 * V2 - X2 * V1

    def calc_LZ(self):
        """
        calculates specific angular Momentum in z-Direction, using LZ_spec
        as an intermediate result.

        | Required columns: ``X1``, ``X2``, ``V1``, ``V2``
        | Intermediate columns: ``LZ_spec``
        | Output columns: ``LZ``
        """
        if "LZ_spec" not in self.columns:
            self.calc_LZ_spec()
        self["LZ"] = self["M"] * self["LZ_spec"]

    def calc_L_spec(self):
        """
        calculate full specific angular Momentum vector.

        | Required columns: ``X1``, ``X2``, ``X3``, ``V1``, ``V2``, ``V3``
        | Output columns: ``L_spec`` (norm), ``LX_spec``, ``LY_spec``, 
          ``LZ_spec``
        """
        R = self[["X1", "X2", "X3"]]
        V = self[["V1", "V2", "V3"]]
        
        if not np.allclose(self.COM, (0, 0, 0)):
            R = R - self.COM

        L_spec = np.cross(R, V) 
        self["L_spec"] = np.linalg.norm(L_spec, axis=1) 
        self["LX_spec"] = L_spec[:, 0]
        self["LY_spec"] = L_spec[:, 1]
        self["LZ_spec"] = L_spec[:, 2]

    def calc_L(self, normalize: str = None):
        """
        calculate angular momentum L, and stores the values in LX, LY, LZ,
        and the norm into L.
        
        | Required columns: ``X1``, ``X2``, ``X3``, ``V1``, ``V2``, ``V3``
        | Output columns: ``L`` (norm), ``LX``, ``LY``, ``LZ``

        :param normalize: Optional noramlize angular momentum. Can be ``unit``,
            ``system``, ``mean`` or ``None``.            
            
            ``unit`` normalizes each vector to one, system normalizes such that
            the total angular momentum of the system is 1
            
            ``system`` normalizes the entire system to a total angular momentum
            of one            
            
            ``mean`` normalizes such that the mean of the enitre system is one.
            
            ``None`` leave everything as is
        
        :type normalize: str or None 
        """

        if normalize not in ["unit", "system", "mean", None]:
            raise ValueError(f"normalize must be 'unit', 'system' or False, but is {normalize}")

        R = self[["X1", "X2", "X3"]].values
        V = self[["V1", "V2", "V3"]].values
        
        """if not np.allclose(self.COM, (0, 0, 0)):
            R = R - self.COM.values"""

        L = np.cross(R, V) * self[["M"]].values
        self["L"] = np.linalg.norm(L, axis=1) 
        self["LX"] = L[:, 0]
        self["LY"] = L[:, 1]
        self["LZ"] = L[:, 2]

        if normalize == "unit":
            self.loc[:, ["LX", "LY", "LZ"]] = self.loc[:, ["LX", "LY", "LZ"]] / np.full((3,self.shape[0]), self.loc[:,"L"].values).T
            self.loc[:, "L"] = 1
        elif normalize == "system":
            self.loc[:, ["LX", "LY", "LZ"]] = self.loc[:, ["LX", "LY", "LZ"]] / self.loc[:, ["L"]].values.sum()
            self.loc[:, "L"] = self.loc[:, "L"]/self.loc[:, "L"].sum()
        elif normalize == "mean":
            self.loc[:, ["LX", "LY", "LZ", "L"]] = self.loc[:, ["LX", "LY", "LZ", "L"]] / self.loc[:, "L"].mean()



    def calc_M_over_MT(self):
        """
        calculate M/M_T within the shell below

        | Required columns: ((``X1``, ``X2``, ``X3``) or ``R``), ``M``
        | Intermediate columns: ``R``
        | Output columns: ``M/MT``
        """
        if "R" not in self.columns or "R/RT" not in self.columns:
            self.calc_spherical_coords()
            self.calc_R_over_RT()
        self.sort_values("R/RT", ignore_index=True,inplace=True)
        self["M/MT"] = self["M"]/self["M"].sum()

    def calc_M_over_MT_cum(self):
        if "M/MT" not in self.columns:
            self.calc_M_over_MT()

        self["M/MT_cum"] = self["M/MT"].cumsum()


    def calc_R_over_RT(self, RT: float = None):
        """
        calculate R/R_T within the shell below

        | Required columns: (``X1``, ``X2``, ``X3``) or ``R``
        | Intermediate columns: ``R``
        | Output columns: ``R/RT``
        """
        if "R" not in self.columns:
            self.calc_R()

        if RT is None:
            RT = self["R"].max()

        self["R/RT"] = self["R"]/RT

    def calc_VROT(self, method: str = "pythonbody", sign_nbody: bool = True):
        """
        calculate rotational velocity.

        | Required columns: ``X1``, ``X2``, ``X3``, ``V1``, ``V2``, ``V3``
        | Intermediate columns: ``R``
        | Output columns: ``VROT``
        
        :param method: Optional which method to use, either standard ``nbody`` way or
            the ``pythonbody`` way, which does not require the system to rotate
            along the z-axis.
        :type method: str ["pythonbody" or "nbody"]
        
        :param sign_nbody: Use the sign convention for VROT from nbody
        :type sign_nbody: bool
        """

        if "R" not in self.columns:
            self.calc_R()

        if method == "pythonbody":
            return self._calc_VROT_pythonbody(sign_nbody)
        elif method == "nbody":
            return self._calc_VROT_nbody(sign_nbody)
        else:
            raise ValueError(f"Method must be either 'nbody' or 'pythonbody' but is {method}")
        
    def _calc_VROT_nbody(self, sign_nbody):
        rvxy = self["X1"]*self["V1"] + self["X2"] * self["V2"]
        rxy2 = self["X1"]**2 + self["X2"]**2
        vrot1 = self["V1"] - rvxy * self["X1"]/rxy2
        vrot2 = self["V2"] - rvxy * self["X2"]/rxy2
        self["VROT"] = np.sqrt(vrot1**2 + vrot2**2)
        if sign_nbody:
            mask = (vrot1*self["X2"] - vrot2*self["X1"]) < 0
            self.loc[mask,"VROT"] = - self.loc[mask,"VROT"]

    def _calc_VROT_pythonbody(self, sign_nbody):
        VROT = np.cross( 
                    np.cross(
                        self.loc[:,["X1","X2","X3"]],
                        self.loc[:,["V1","V2","V3"]]
                    )/(self["R"]**2).values.reshape(self.shape[0],1),
                    self.loc[:,["X1","X2","X3"]]
                )
        self["VROTX"] = VROT[:,0]
        self["VROTY"] = VROT[:,1]
        self["VROTZ"] = VROT[:,2]
        XSIGN = 1
        if sign_nbody:
            XSIGN = np.sign(VROT[:,0]*self["X2"]/np.sqrt(self["X1"]**2 + self["X2"]**2) \
                                - VROT[:,1]*self["X1"]/np.sqrt(self["X1"]**2 + self["X2"]**2))
        self["VROT"] = XSIGN * np.linalg.norm(VROT, axis=1)

    def calc_V(self):
        """
        calculates absolute velocity.

        | Required columns: ``V1``, ``V2``, ``V3``
        | Output columns: ``V``
        """
        self["V"] = np.linalg.norm(self[["V1", "V2", "V3"]],axis=1)

    def calc_NEIGHBOUR_RHO(self,
                           n_neigh: int = 80,
                           c_func: str = None,
                           omp_n_procs: int = None,
                           input_columns: list = ["M", "X1", "X2", "X3"],
                           output_columns: list = ["NEIGHBOUR_RHO_N", "NEIGHBOUR_RHO_M"],
                           ):
        if len(input_columns) != 4 or len(output_columns) != 2:
            raise ValueError(f"Input columns length must be equal to 4 (is {len(input_columns)})"
                             f"Output columns length must be equal to 2 (is {len(output_columns)})"
                             )
        """
        calculate neighbour density both in number and mass, meaning in this 
        case simply 1/(4/3*pi*r_bar^3) and m_bar/(4/3*pi*r_bar^3)
        where r_bar is the average distance and m_bar the average mass of the
        neighbours to the star.

        :param n_neigh: number of neighbour to average over
        :type n_neigh: int
        :param c_func: type for ffi to use
        :type c_func: str: ["omp", "ocl", "unthreaded"]
        :param omp_n_procs: number of threads omp is supposed to use.
            Automatically sets ``c_func = "omp"``.
        :type omp_n_procs: int

        | Required columns: ``M``, ``X1``, ``X2``, ``X3``
        | Output columns: ``NEIGHBOUR_RHO_N``, ``NEIGHBOUR_RHO_M``
        """
        res = ffi.neighbour_density(self[input_columns],
                                    n_neigh=n_neigh, c_func=c_func, omp_n_procs=omp_n_procs,
                                    input_columns=input_columns)

        self[output_columns[0]] = res[0]
        self[output_columns[1]] = res[1]

    def _calc_NEIGHBOUR_RHO_N(self, n_neigh: int = 80, c_func: str = None, omp_n_procs: int = None):
        self.calc_NEIGHBOUR_RHO(n_neigh=n_neigh, c_func=c_func, omp_n_procs=omp_n_procs)
    
    def _calc_NEIGHBOUR_RHO_M(self, n_neigh: int = 80, c_func: str = None, omp_n_procs: int = None):
        self.calc_NEIGHBOUR_RHO(n_neigh=n_neigh, c_func=c_func, omp_n_procs=omp_n_procs)

    def _calc_GOODWIN2004_FRACTALITY(self):
        res = ffi.goodwin_fractality(self)
        return res

    def calc_NEIGHBOUR_AVG_DIST(self, n_neigh: int = None, recalc: bool = False):
        """
        calculate average distance to the neighbours based on 
        ``NEIGHBOUR_RHO_N``. If ``NEIGHBOUR_RHO_N`` doesn't exist yet,
        it will be calculated using ``n_neigh``.

        :param n_neigh: number of neighbour to average over. Will be ignored if
            ``NEIGHBOUR_RHO_N`` already exists and ``recalc = False``
        :type n_neigh: int
        :param recalc: force recalculation of ``NEIGHBOUR_RHO_N``
        :type recalc: bool

        | Required columns: ``NEIGHBOUR_RHO_N`` or (``M``, ``X1``, ``X2``, ``X3``)
        | Intermediary columns: ``NEIGHBOUR_RHO_N`` (if not present before)
        | Output columns: ``NEIGHBOUR_AVG_DIST``
        """
        if n_neigh is not None and "NEIGHBOUR_RHO_N" in self.columns and recalc == False:
            print("Warning: n_neigh was passed, but NEIGHBOUR_RHO_N is already calculated. Ignoring n_neigh.")
        if ("NEIGHBOUR_RHO_N" not in self.columns) or recalc:
            self.calc_NEIGHBOUR_RHO(n_neigh=(n_neigh if n_neigh is not None else 80))

        self.loc[:,"NEIGHBOUR_AVG_DIST"] = (1/self.loc[:,"NEIGHBOUR_RHO_N"]/(4/3)/np.pi)**(1/3)



    def calc_VROT_CUMMEAN(self):
        if "VROT" not in self.columns:
            self.calc_VROT()

        self["VROT_CUMMEAN"] = ffi.cummean(self["VROT"].values)

    def radius_from_mass_fraction(self, mass_fraction = 0.5):
        """
        get radius for given mass fraction

        :param mass_fraction: Fraction of mass to get the radius of.
        :type mass_fraction: float
        :returns: radius for given massen fraction
        :rtype: float
        """
        #adjusted = False
        #COM = None
        #if not np.allclose(self.COM, np.zeros(3)):
        #    COM = self.COM
        #    self.adjust_COM()
        #    adjusted = True

        if "M/MT_cum" not in self.columns:
            self.calc_M_over_MT_cum()
        if "R" not in self.columns:
            self.calc_spherical_coords()

        #if adjusted:
        #    self[:,["X1", "X2", "X3"]] = self[:,["X1", "X2", "X3"]] + COM
        
        #return self[self.index == np.argmin(np.abs(self["M/MT_cum"] - mass_fraction))]["R"].values[0]
        return self.iloc[np.argmin(np.abs(self["M/MT_cum"] - mass_fraction))]["R"]

    def get_shells(self, *args, **kwargs):
        """
        See :doc:`ffi` ``calc_shells()``
        """
        return ffi.calc_shells(self, *args, **kwargs)
    def get_cells2d(self, *args, **kwargs):
        """
        See :doc:`ffi` ``calc_cells2d()``
        """
        return ffi.calc_cells2d(self, *args, **kwargs)
    def get_cells3d(self, *args, **kwargs):
        """
        See :doc:`ffi` ``calc_cells3d()``
        """
        return ffi.calc_cells3d(self, *args, **kwargs)
