#ifndef NEIGHBOUR_H
#define NEIGHBOUR_H

#include <immintrin.h>

#if OCL == 1
#include "ocl.h"
/*extern cl_program ocl_program_neighbour_density;
extern cl_kernel ocl_kernel_neighbour_density;
int ocl_init_neighbour_density(void);
void ocl_free_neigbour_density(void);*/
#endif

void get_neighbour_list(float *X1, float *X2, float *X3, int n, int nneigh, float **distance_list, int **index_list);
int calc_neighbour_density(float *M,
                            float *X1,
                            float *X2,
                            float *X3,
                            float *neighbour_density_n,
                            float *neighbour_density_m,
                            float **dist_list,
                            int **indx_list,
                            int n,
                            int nneigh
                            );
int calc_neighbour_density_goodwin2004(float *M,
                          float *X1,
                          float *X2,
                          float *X3,
                          float *neighbour_density,
                          float **dist_list,
                          int **indx_list,
                          int n
                          );

void init_dist_indx_list(float **dist_list, int **indx_list, int n, int nneigh);
float **init_dist_list(float **dist_list, int n, int nneigh);
int **init_indx_list(int **indx_list, int n, int nneigh);
void reset_dist_indx_list(float **dist_list, int **indx_list, int n, int nneigh);
void reset_dist_list(float **dist_list, int n, int nneigh);
void reset_indx_list(int **indx_list, int n, int nneigh);
void free_dist_indx_list(float **dist_list, int **indx_list, int n);
void free_dist_list(float **dist_list, int n);
void free_indx_list(int **indx_list, int n);
#if OCL
//double neighbour_density_ocl(float *m, float *x1, float *x2, float *x3, float *EPOT, int n);
#endif

#endif
