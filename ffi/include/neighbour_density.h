#ifndef NEIGHBOUR_DENSITY_H
#define NEIGHBOUR_DENSITY_H

#if OCL
#include "ocl.h"
extern cl_program ocl_program_neighbour_density;
extern cl_kernel ocl_kernel_neighbour_density;
void ocl_free_neighbour_density(void);
int ocl_init_neighbour_density(void);
#endif

int neighbour_density_omp_old(float *m,
	   					   float *x1,
						   float *x2,
						   float *x3,
						   float *neighbour_density_n,
						   float *neighbour_density_m,
						   int n_neigh,
						   int n_tot,
						   int *n_procs);
int neighbour_density_unthreaded(float *m,
	   					   float *x1,
						   float *x2,
						   float *x3,
						   float *neighbour_density_n,
						   float *neighbour_density_m,
						   int n_neigh,
						   int n_tot);
int neighbour_density_ocl(float *m,
	   					   float *x1,
						   float *x2,
						   float *x3,
						   float *neighbour_density_n,
						   float *neighbour_density_m,
						   int n_neigh,
						   int n_tot);

#endif
