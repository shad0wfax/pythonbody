float *get_goodwin2004_fractality(
	    float *goodwin_fractality,		
        float *M,                                                                  
        float *X1,                                                                 
        float *X2,                                                                 
        float *X3,                                                                 
        float *neighbour_density,                                                  
        float **dist_list,                                                         
        int **indx_list,                                                         
        int *nneigh,                                                               
        int n);

int count_boxes_with_stars(float *X1, float *X2, float *X3, float min, float max, int n_boxes_per_dim, int n);
