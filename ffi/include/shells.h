void calc_shells(float *col,
	   	         float *cols,
				 float *shell_boundaries,
				 float *shell_data,
				 float *shell_data_std,
				 int *shell_particle_count,
				 int n,
				 int nshellbounds,
				 int ncols);
