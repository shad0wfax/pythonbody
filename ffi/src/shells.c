#include <stdio.h>

#if OMP
#include <omp.h>
#endif

/* TODO improve this algo!!! */
int _get_shell_idx(float *shell_boundaries, int nshellbounds, float val) {
	int guess = (int) ((val*(nshellbounds - 1)) + 1);
	if (val > shell_boundaries[guess - 1] && val < shell_boundaries[guess] && guess > 0 && guess < nshellbounds) {
		return guess - 1;
	}
	for (int i = 1; i < nshellbounds; i++) {
		if ((val > shell_boundaries[i-1]) && (val <= shell_boundaries[i]) ) {
			return i - 1;
		}
	}
	if (val > shell_boundaries[nshellbounds]) {
		return -1;
	}
	return -2;
}

void calc_shells(float *col,
	   	         float *cols,
				 float *shell_boundaries,
				 float *shell_data,
				 float *shell_data_std,
				 int *shell_particle_count,
				 int n,
				 int nshellbounds,
				 int ncols) {
	#if OMP
	#pragma omp parallel
	{
	#pragma omp for
	#endif
	for (int i = 0; i < n; i++) {
		int shell_idx = _get_shell_idx(shell_boundaries, nshellbounds, col[i]);
		if (shell_idx < 0) {
			#if DEBUG
			printf("shells.c/calc_shells: Couldn't get shell index for val = %.03e particle_id %d (err = %d)\n", col[i], i, shell_idx);
			#endif
			continue;
		}
		#if DEBUG
		printf("shells.c/calc_shells: shell index for val = %.03e is %d (limit = %f)\n", col[i], shell_idx, shell_boundaries[shell_idx+1]);
		#endif

		for (int j = 0; j < ncols; j++) {
			#if OMP
			#pragma omp atomic
			#endif
			shell_data[shell_idx*ncols + j] += cols[i*ncols + j];
		}
		#if OMP
		#pragma omp atomic
		#endif
		shell_particle_count[shell_idx] += 1;
	}
	#if OMP
	}
	#endif

	if (shell_data_std != NULL) {
        #if DEBUG
		printf("shells.c/calc_shells: STD Calculating standard deviation\n");
        #endif
		#if OMP
		#pragma omp parallel
		{
		#pragma omp for
		#endif
		for (int i = 0; i < n; i++) {
			int shell_idx = _get_shell_idx(shell_boundaries, nshellbounds, col[i]);
			if (shell_idx < 0) {
				#if DEBUG
				printf("shells.c/calc_shells: STD Couldn't get shell index for val = %.03e particle_id %d (err = %d)\n", col[i], i, shell_idx);
				#endif
				continue;
			}
			for (int j = 0; j < ncols; j++) {
				if (shell_particle_count[shell_idx] > 0){
					#if OMP
					#pragma omp atomic
					#endif
					shell_data_std[shell_idx*ncols + j] += ((shell_data[shell_idx*ncols + j]/shell_particle_count[shell_idx] - cols[i*ncols + j]) * (shell_data[shell_idx*ncols + j]/shell_particle_count[shell_idx] - cols[i*ncols + j]))/shell_particle_count[shell_idx];
				}
			}

		}
		#if OMP
		}
		#endif
	}

	#if DEBUG
	for (int i = 0; i < nshellbounds - 1; i++) {
		printf("shells.c/calc_shells: %f N = %d ", shell_boundaries[i+1], shell_particle_count[i]);
		for (int j = 0; j < ncols; j++) {
			printf("%f ", shell_data[i*ncols + j]);
		}
		printf("\n");
	}
	#endif
}	

void calc_cells2d(float *colA,
                  float *colB,
	   	          float *cols,
				  float *cell_data,
				  float *cell_data_std,
				  int *cell_particle_count,
                  float dim_min_A,
                  float dim_max_A,
                  float dim_min_B,
                  float dim_max_B,
				  int n,
				  int ncells,
                  int ncols) {
	#if OMP
	#pragma omp parallel
	{
	#pragma omp for
	#endif
	for (int i = 0; i < n; i++) {
        int cell_idx_x = (colA[i] - dim_min_A)/(dim_max_A - dim_min_A)*ncells; 
        int cell_idx_y = (colB[i] - dim_min_B)/(dim_max_B - dim_min_B)*ncells; 
		if (cell_idx_x < 0 || cell_idx_x >= ncells || cell_idx_y < 0 || cell_idx_y >= ncells) {
			#if DEBUG
			printf("shells.c/calc_cells2d: Couldn't get cell index for valA = %.03e, valB = %.03e particle_id %d (err = %d,%d)\n", colA[i], colB[i], i, cell_idx_x, cell_idx_y);
			#endif
			continue;
		}
		#if DEBUG
		printf("shells.c/calc_cells2d: cell index for colA = %.03e is %d, colB = %.03e is %d\n", colA[i], cell_idx_x, colB[i], cell_idx_y);
		#endif

		for (int j = 0; j < ncols; j++) {
			#if OMP
			#pragma omp atomic
			#endif
			cell_data[(ncells*cell_idx_y + cell_idx_x)*ncols + j] += cols[i*ncols + j];
		}
		#if OMP
		#pragma omp atomic
		#endif
		cell_particle_count[ncells*cell_idx_y + cell_idx_x] += 1;
	}
	#if OMP
	}
	#endif

	if (cell_data_std != NULL) {
        #if DEBUG
		printf("shells.c/calc_cells2d: STD Calculating standard deviation\n");
        #endif
		#if OMP
		#pragma omp parallel
		{
		#pragma omp for
		#endif
		for (int i = 0; i < n; i++) {
			int cell_idx_x = (colA[i] - dim_min_A)/(dim_max_A - dim_min_A)*ncells; 
			int cell_idx_y = (colB[i] - dim_min_B)/(dim_max_B - dim_min_B)*ncells; 
			int cell_n_idx = ncells*cell_idx_y + cell_idx_x;
			if (cell_idx_x < 0 || cell_idx_x >= ncells || cell_idx_y < 0 || cell_idx_y >= ncells) {
				#if DEBUG
				printf("shells.c/calc_cells2d: STD Couldn't get cell index for valA = %.03e, valB = %.03e particle_id %d (err = %d,%d)\n", colA[i], colB[i], i, cell_idx_x, cell_idx_y);
				#endif
				continue;
			}
			for (int j = 0; j < ncols; j++) {
				if (cell_particle_count[cell_n_idx] > 0){
					int cell_idx = (ncells*cell_idx_y + cell_idx_x)*ncols + j;
					#if OMP
					#pragma omp atomic
					#endif
					cell_data_std[cell_idx] += ((cell_data[cell_idx]/cell_particle_count[cell_n_idx] - cols[i*ncols + j]) * (cell_data[cell_idx]/cell_particle_count[cell_n_idx] - cols[i*ncols + j]))/cell_particle_count[cell_n_idx];
				}
			}

		}
		#if OMP
		}
		#endif
	}

	#if DEBUG
	for (int i = 0; i < ncells; i++) {
		for (int j = 0; j < ncells; j++) {
			printf("x = %d, y = %d ", i,j);
			for (int k = 0; k < ncols; k++) {
				printf("%f ", cell_data[(ncells*i + j) * ncols + k]);
			}
			printf("\n");
		}
	}
	#endif
}



void calc_cells3d(float *colA,
                  float *colB,
                  float *colC,
	   	          float *cols,
				  float *cell_data,
				  float *cell_data_std,
				  int *cell_particle_count,
                  float dim_min_A,
                  float dim_max_A,
                  float dim_min_B,
                  float dim_max_B,
                  float dim_min_C,
                  float dim_max_C,
				  int n,
				  int ncells,
                  int ncols) {
	#if OMP
	#pragma omp parallel
	{
	#pragma omp for
	#endif
	for (int i = 0; i < n; i++) {
        int cell_idx_x = (colA[i] - dim_min_A)/(dim_max_A - dim_min_A)*ncells; 
        int cell_idx_y = (colB[i] - dim_min_B)/(dim_max_B - dim_min_B)*ncells; 
        int cell_idx_z = (colC[i] - dim_min_C)/(dim_max_C - dim_min_C)*ncells; 
		if (cell_idx_x < 0 || cell_idx_x >= ncells 
				|| cell_idx_y < 0 || cell_idx_y >= ncells
				|| cell_idx_z < 0 || cell_idx_z >= ncells) {
			#if DEBUG
			printf("shells.c/calc_cells2d: Couldn't get cell index for valA = %.03e, valB = %.03e particle_id %d (err = %d,%d)\n", colA[i], colB[i], i, cell_idx_x, cell_idx_y);
			#endif
			continue;
		}
		#if DEBUG
		printf("shells.c/calc_cells2d: cell index for colA = %.03e is %d, colB = %.03e is %d\n", colA[i], cell_idx_x, colB[i], cell_idx_y);
		#endif

		for (int j = 0; j < ncols; j++) {
			#if OMP
			#pragma omp atomic
			#endif
			cell_data[(ncells*ncells*cell_idx_z + ncells*cell_idx_y + cell_idx_x)*ncols + j] += cols[i*ncols + j];
		}
		#if OMP
		#pragma omp atomic
		#endif
		cell_particle_count[ncells*ncells*cell_idx_z + ncells*cell_idx_y + cell_idx_x] += 1;
	}
	#if OMP
	}
	#endif

	if (cell_data_std != NULL) {
        #if DEBUG
		printf("shells.c/calc_cells2d: STD Calculating standard deviation\n");
        #endif
		#if OMP
		#pragma omp parallel
		{
		#pragma omp for
		#endif
		for (int i = 0; i < n; i++) {
			int cell_idx_x = (colA[i] - dim_min_A)/(dim_max_A - dim_min_A)*ncells; 
			int cell_idx_y = (colB[i] - dim_min_B)/(dim_max_B - dim_min_B)*ncells; 
        	int cell_idx_z = (colC[i] - dim_min_C)/(dim_max_C - dim_min_C)*ncells; 
			int cell_n_idx = ncells*ncells*cell_idx_z + ncells*cell_idx_y + cell_idx_x;
			if (cell_idx_x < 0 || cell_idx_x >= ncells 
					|| cell_idx_y < 0 || cell_idx_y >= ncells
					|| cell_idx_z < 0 || cell_idx_z >= ncells) {
				#if DEBUG
				printf("shells.c/calc_cells2d: STD Couldn't get cell index for valA = %.03e, valB = %.03e particle_id %d (err = %d,%d)\n", colA[i], colB[i], i, cell_idx_x, cell_idx_y);
				#endif
				continue;
			}
			for (int j = 0; j < ncols; j++) {
				if (cell_particle_count[cell_n_idx] > 0){
					int cell_idx = (ncells*ncells*cell_idx_z + ncells*cell_idx_y + cell_idx_x)*ncols + j;
					#if OMP
					#pragma omp atomic
					#endif
					cell_data_std[cell_idx] += ((cell_data[cell_idx]/cell_particle_count[cell_n_idx] - cols[i*ncols + j]) * (cell_data[cell_idx]/cell_particle_count[cell_n_idx] - cols[i*ncols + j]))/cell_particle_count[cell_n_idx];
				}
			}

		}
		#if OMP
		}
		#endif
	}
}
