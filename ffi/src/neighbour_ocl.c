#if OCL == 1
#include <stdio.h>
#include <stdlib.h>

#include "../include/ocl.h"

cl_program ocl_program_neighbour_density;
cl_kernel ocl_kernel_neighbour_density;

const char *kernel_source_neighbour_density =                    "\n" \
"__kernel void neighbour_density_kernel(                          \n" \
"                            __global float *m,                   \n" \
"                            __global float *x1,                  \n" \
"                            __global float *x2,                  \n" \
"                            __global float *x3,                  \n" \
"                            __global float **dist_list,          \n" \
"                            __global int **indx_list,            \n" \
"                            int nneigh,                          \n" \
"                            int n)                               \n" \
"{                                                                \n" \
"    //Get our global thread ID                                   \n" \
"    int id = get_global_id(0);                                   \n" \
"    int i  = id;                                                 \n" \
"    float EPOT_i = 0.0;                                          \n" \
"    //Make sure we do not go out of bounds                       \n" \
"    if (id < n) {                                                \n" \
"       for (int j = 0; j < n; j++) {                             \n" \
"		   if (i == j) continue;                                  \n" \
"           float dist = sqrt( (x1[i] - x1[j])*(x1[i] - x1[j]) + (x2[i] - x2[j])*(x2[i] - x2[j]) + (x3[i] - x3[j])*(x3[i] - x3[j]));\n" \
"           EPOT_i +=  -m[i]*m[j]/dist;                           \n" \
"       }                                                         \n" \
"      //EPOT[id] = EPOT_i;                                         \n" \
"    }                                                            \n" \
"}\n\n";


int ocl_init_neighbour_density(void) {
	cl_int err;
    // Create the compute program from the source buffer
	/*char source_code[1000];
	//ocl_read_source("pythonbody/ffi/src/grav_pot_ocl.c", source_code);
	ocl_read_source("src/grav_pot_ocl.c", source_code);
	const char *sc = source_code;*/
    ocl_program_neighbour_density = clCreateProgramWithSource(ocl_context, 1,
                            (const char **) & kernel_source_neighbour_density, NULL, &err);
	//free(source_code);
    CL_SUCCESS_OR_RETURN(err, "clCreateProgramWithSource");

    // Build the program executable
    clBuildProgram(ocl_program_neighbour_density, 1, ocl_device_id, NULL, NULL, NULL);

	#if DEBUG
    char build_log[4096];
    err = clGetProgramBuildInfo(ocl_program_neighbour_density, ocl_device_id[0], CL_PROGRAM_BUILD_LOG, (size_t) 4096, build_log, NULL);
	printf("BUILD_LOG\n");
    printf("%s", build_log);
	#endif
    CL_SUCCESS_OR_RETURN(err, "clGetProgramBuildInfo");

    // Create the compute kernel in the program we wish to run
    ocl_kernel_neighbour_density = clCreateKernel(ocl_program_neighbour_density, "grav_pot_kernel", &err);
    CL_SUCCESS_OR_RETURN(err, "clCreateKernel");
    return CL_SUCCESS;
}
    
void ocl_free_neighbour_density(void) {
    clReleaseProgram(ocl_program_neighbour_density);
    clReleaseKernel(ocl_kernel_neighbour_density);
}

int neighbour_density_ocl (
	            float *m,
                float *x1,
                float *x2,
                float *x3,
                float **dist_list,
                int **indx_list,
				int nneigh,
                int n
                )
{
    cl_mem l_m;
    cl_mem l_x1;
    cl_mem l_x2;
    cl_mem l_x3;
    cl_mem l_dist_list;
	cl_mem l_indx_list;
     
    size_t bytes = n*sizeof(float);
    
    cl_int err;
 
    // Number of work items in each local work group
    /*size_t globalSize[2], localSize[2];
	int ndim = 2;
	for (int i = 0; i < 2; i++) {
		globalSize[i] = (n/32 + 1)*32;
	}
	localSize[0] = 4;
	localSize[1] = 32;*/
	int ndim = 1;
	size_t globalSize, localSize;
	globalSize = (unsigned int) n;
	localSize = 32;

 
    // Number of total work items - localSize must be devisor
     
    // Create the input and output arrays in device memory for our calculation
    l_m = clCreateBuffer(ocl_context, CL_MEM_READ_ONLY, bytes, NULL, &err);
    l_x1 = clCreateBuffer(ocl_context, CL_MEM_READ_ONLY, bytes, NULL, &err);
    l_x2 = clCreateBuffer(ocl_context, CL_MEM_READ_ONLY, bytes, NULL, &err);
    l_x3 = clCreateBuffer(ocl_context, CL_MEM_READ_ONLY, bytes, NULL, &err);
    l_dist_list = clCreateBuffer(ocl_context, CL_MEM_READ_WRITE, n*sizeof(float*), NULL, &err);
    l_indx_list = clCreateBuffer(ocl_context, CL_MEM_READ_WRITE, n*sizeof(int*), NULL, &err);
	CL_SUCCESS_OR_RETURN(err, "clCreateBuffer");
	for (int i = 0; i < n; i++) {
		clCreateBuffer(ocl_context, CL_MEM_READ_WRITE, nneigh*sizeof(float), l_dist_list[i], &err);
		clCreateBuffer(ocl_context, CL_MEM_READ_WRITE, nneigh*sizeof(int), l_indx_list[i], &err);
	}
	CL_SUCCESS_OR_RETURN(err, "clCreateBuffer float**");
    
    err = clEnqueueWriteBuffer(ocl_queue, l_m, CL_TRUE, 0,
                                   bytes, m, 0, NULL, NULL);
    err |= clEnqueueWriteBuffer(ocl_queue, l_x1, CL_TRUE, 0,
                                   bytes, x1, 0, NULL, NULL);
    err |= clEnqueueWriteBuffer(ocl_queue, l_x2, CL_TRUE, 0,
                                   bytes, x2, 0, NULL, NULL);
    err |= clEnqueueWriteBuffer(ocl_queue, l_x3, CL_TRUE, 0,
                                   bytes, x3, 0, NULL, NULL);
    err |= clEnqueueWriteBuffer(ocl_queue, l_EPOT, CL_TRUE, 0,
                                   bytes, EPOT, 0, NULL, NULL);
	CL_SUCCESS_OR_RETURN(err, "clEngueueWriteBuffer");
	err = 0;
	for (int i = 0; i < n; i++) {
		err |= clEnqueueWriteBuffer(ocl_queue, l_dist_list[i], CL_TRUE, 0,
				                    nneigh*sizeof(float), dist_list[i], 0,NULL,NULL);
		err |= clEnqueueWriteBuffer(ocl_queue, l_indx_list[i], CL_TRUE, 0,
				                    nneigh*sizeof(float), indx_list[i], 0,NULL,NULL);
	}

	CL_SUCCESS_OR_RETURN(err, "clEngueueWriteBuffer");
    
    /*// Set the arguments to our compute kernel
    err  = clSetKernelArg(ocl_kernel_grav_pot, 0, sizeof(cl_mem), &l_m);
    err |= clSetKernelArg(ocl_kernel_grav_pot, 1, sizeof(cl_mem), &l_x1);
    err |= clSetKernelArg(ocl_kernel_grav_pot, 2, sizeof(cl_mem), &l_x2);
    err |= clSetKernelArg(ocl_kernel_grav_pot, 3, sizeof(cl_mem), &l_x3);
    err |= clSetKernelArg(ocl_kernel_grav_pot, 4, sizeof(cl_mem), &l_EPOT);
    err |= clSetKernelArg(ocl_kernel_grav_pot, 5, sizeof(int), &n);
	
	CL_SUCCESS_OR_RETURN(err, "clSetKernelArg");
 
    // Execute the kernel over the entire range of the data set 
    err = clEnqueueNDRangeKernel(ocl_queue, ocl_kernel_grav_pot, ndim, NULL, &globalSize, &localSize,
                                                              0, NULL, NULL);
	CL_SUCCESS_OR_RETURN(err, "clEnqueueNDRangeKernel");
 
    // Wait for the command queue to get serviced before reading back results
    clFinish(ocl_queue);
 
    // Read the results from the device
    clEnqueueReadBuffer(ocl_queue, l_EPOT, CL_TRUE, 0,
                                bytes, EPOT, 0, NULL, NULL );
 
    //Sum up vector c and print result divided by n, this should equal 1 within error*/
 
    // release OpenCL resources
    clReleaseMemObject(l_m);
    clReleaseMemObject(l_x1);
    clReleaseMemObject(l_x2);
    clReleaseMemObject(l_x3);
	for (int i = 0; i < n; i++) {
		clReleaseMemObject(l_dist_list[i]);
		clReleaseMemObject(l_indx_list[i]);
	}
    clReleaseMemObject(l_dist_list);
    clReleaseMemObject(l_indx_list);
}
#endif

