#include "../include/ocl.h"
#include <stdio.h>
#include <stdlib.h>


#if OCL

cl_platform_id ocl_platform_id[MAX_PLATFORMS];
cl_device_id ocl_device_id[MAX_DEVICES];
cl_context ocl_context;
cl_command_queue ocl_queue;
int ocl_below_20 = 0;

int ocl_init(int *p_id, int *d_id) {
	cl_int err;
	int p_id_was_null = 0;
	int d_id_was_null = 0;
	if (p_id == NULL) {
		p_id = (int *) malloc(sizeof(int));
		*p_id = 0;
		p_id_was_null = 1;
	}
	if (d_id == NULL) {
		d_id = (int *) malloc(sizeof(int));
		*d_id = 0;
		d_id_was_null = 1;
	}
	for (int i = 0; (i < MAX_PLATFORMS) || (i < MAX_DEVICES); i++) {
		if ( i < MAX_PLATFORMS ) { ocl_platform_id[i] = 0;}
		if ( i < MAX_DEVICES ) { ocl_device_id[i] = 0;}
	}

	err = clGetPlatformIDs(MAX_PLATFORMS, ocl_platform_id, NULL);
	CL_SUCCESS_OR_RETURN(err, "clGetPlatfromIDs");

	#if DEBUG
	printf("OCL: loading platform id %d\n", *p_id);
	#endif
	err = clGetDeviceIDs(ocl_platform_id[*p_id], CL_DEVICE_TYPE_ALL, MAX_DEVICES, ocl_device_id, NULL);
    if (err != CL_SUCCESS) {
		printf("Warning, need to use CPU as fallback device\n");
        err = clGetDeviceIDs(ocl_platform_id[*p_id], CL_DEVICE_TYPE_CPU, MAX_DEVICES, ocl_device_id, NULL);
    }
	CL_SUCCESS_OR_RETURN(err, "clGetDeviceIDs");

	#if DEBUG
	printf("OCL: loading device id %d\n", *d_id);
	#endif
	ocl_context = clCreateContext(0, 1, &ocl_device_id[*d_id], NULL, NULL, &err);
	CL_SUCCESS_OR_RETURN(err, "clCreateContext");

	/* Check OpenCL version below 2.0 */
	/*char buf[100];
	err = clGetPlatformInfo(ocl_platform_id[*p_id], CL_PLATFORM_VERSION, 100, &buf, NULL);
	CL_SUCCESS_OR_RETURN(err, clGetPlatformInfo);
	printf("%s\n",buf);*/
	
	#if CL_TARGET_OPENCL_VERSION > 120
	ocl_queue = clCreateCommandQueueWithProperties(ocl_context, ocl_device_id[*d_id], 0, &err);
	#else
	ocl_queue = clCreateCommandQueue(ocl_context, ocl_device_id[*d_id], 0, &err);
	#endif
	CL_SUCCESS_OR_RETURN(err, "clCreateQueue");

	if (p_id_was_null) {	
		free(p_id);
	}
	if (d_id_was_null) {
		free(d_id);
	}

	return CL_SUCCESS;
}

void ocl_free(void) {
	clReleaseCommandQueue(ocl_queue);
    clReleaseContext(ocl_context);
}

#include <unistd.h>
int ocl_read_source(char *filename, char *source_code) {
	FILE *fp;
	long filesize;

	fp = fopen(filename, "rb");

	if (!fp) {
		perror("Couldn't open the file.");
		printf("Filename \"%s\"\n", filename);
		char buf[255];
		getcwd(buf, sizeof(buf));
		printf("CWD \"%s\"\n", buf);

		exit(1);
	}

	fseek(fp, 0L, SEEK_END);
	filesize = ftell(fp);
	rewind(fp);

	//source_code = calloc(1, filesize + 1);
	//source_code = (char *) malloc(filesize + 1);
	if (!source_code) {
		perror("memory allocation failed");
		exit(1);
	}

	if ( 1!= fread(source_code, filesize, 1 , fp) ) {
		fclose(fp);
		fputs("error reading file", stderr);
	}

	fclose(fp);

	return 0;
}

#endif
