#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <omp.h>
#include <immintrin.h>

#include "../include/grav_pot.h"

#if OCL == 1
#include "../include/ocl.h"
#endif

double grav_pot(float *m, float *x1, float *x2, float *x3, float *EPOT, int n) {
    #if OCL
    return grav_pot_ocl(m,x1,x2,x3,EPOT,n);
    #elif OMP
    return grav_pot_omp(m,x1,x2,x3,EPOT,n);
    #else
    return grav_pot_unthreaded(m,x1,x2,x3,EPOT,n);
    #endif
}

#if OMP == 1
double grav_pot_omp(float *m, float *x1, float *x2, float *x3, float *epot, int n) {
	#if AVX
	__m256* M = (__m256*) m;
	__m256* X1 = (__m256*) x1;
	__m256* X2 = (__m256*) x2;
	__m256* X3 = (__m256*) x3;
	#endif
	#pragma omp parallel
	{
		#pragma omp for
		for (int i = 0; i < n; i++) {
			#if AVX
			__m256 X1_i = _mm256_set1_ps(x1[i]);
			__m256 X2_i = _mm256_set1_ps(x2[i]);
			__m256 X3_i = _mm256_set1_ps(x3[i]);
			__m256 M_i = _mm256_set1_ps(m[i]);
			_grav_pot_inner_loop_avx(M,X1,X2,X3,m, x1, x2, x3, M_i, X1_i, X2_i, X3_i, epot, i,n);
			#else
			for (int j = 0; j < n; j++) {
				if ( i == j ) { continue; }
				float dist = sqrt((x1[i] - x1[j])*(x1[i] - x1[j]) + (x2[i] - x2[j])*(x2[i] - x2[j]) + (x3[i] - x3[j])*(x3[i] - x3[j]));
				float epot_ij = -m[i]*m[j]/dist;
				epot[i] += epot_ij;
			}
			#endif
			
		}
	}
}
#endif

#if AVX
void _grav_pot_inner_loop_avx(
		__m256 *M,
		__m256 *X1,
		__m256 *X2,
		__m256 *X3,
		float *m,
		float *x1,
		float *x2,
		float *x3,
		__m256 M_i,
		__m256 X1_i,
		__m256 X2_i,
		__m256 X3_i,
		float *epot,
		int i,
		int n
		) {
	__m256 AVX_EPOT = _mm256_set1_ps(0.0);
	for (int j = 0; j < (int) n/8; j++) {
		__m256 dist_x1 = _mm256_sub_ps(X1_i,X1[j]);
		__m256 dist_x2 = _mm256_sub_ps(X2_i,X2[j]);
		__m256 dist_x3 = _mm256_sub_ps(X3_i,X3[j]);

		dist_x1 = _mm256_mul_ps(dist_x1, dist_x1);
		dist_x2 = _mm256_mul_ps(dist_x2, dist_x2);
		dist_x3 = _mm256_mul_ps(dist_x3, dist_x3);
		__m256 dist = _mm256_add_ps(dist_x1, dist_x2);
		dist = _mm256_add_ps(dist, dist_x3);
		dist = _mm256_sqrt_ps(dist);

		__m256 epot_ij = _mm256_mul_ps(M_i, M[j]);
		epot_ij = _mm256_div_ps(epot_ij,dist);

		/* Disabling self gravity */			
		__m256 mask = _mm256_cmp_ps(dist, _mm256_set1_ps(0.0), _CMP_NEQ_OQ);
		epot_ij = _mm256_and_ps(epot_ij, mask);

		/* sore results */
		AVX_EPOT = _mm256_add_ps(AVX_EPOT, epot_ij);

	}
	/* Summing all together */
	float *EPOT_float = (float *) &AVX_EPOT;
	float res = 0.0;
	for (int j = 0; j < 8; j++) {
		res += EPOT_float[j];
	}
	for (int j = ((int) n/8)*8; j < n; j++) {
		if (i == j) { continue ;}
		float dist = sqrt((x1[i] - x1[j])*(x1[i] - x1[j]) + (x2[i] - x2[j])*(x2[i] - x2[j]) + (x3[i] - x3[j])*(x3[i] - x3[j]));
		float epot_ij = m[i]*m[j]/dist;
		res += epot_ij; 

	}
	epot[i] -= res;
}
#endif

double grav_pot_unthreaded(float *m, float *x1, float *x2, float *x3, float *epot, int n) {
	#if AVX
	__m256* M = (__m256*) m;
	__m256* X1 = (__m256*) x1;
	__m256* X2 = (__m256*) x2;
	__m256* X3 = (__m256*) x3;
	#endif

	for (int i = 0; i < n; i++) {
		#if AVX
		__m256 X1_i = _mm256_set1_ps(x1[i]);
		__m256 X2_i = _mm256_set1_ps(x2[i]);
		__m256 X3_i = _mm256_set1_ps(x3[i]);
		__m256 M_i = _mm256_set1_ps(m[i]);
		_grav_pot_inner_loop_avx(M,X1,X2,X3,m,x1,x2,x3, M_i, X1_i, X2_i, X3_i, epot, i,n);
		#else
		for (int j = i+1; j < n; j++) {
			float dist = sqrt((x1[i] - x1[j])*(x1[i] - x1[j]) + (x2[i] - x2[j])*(x2[i] - x2[j]) + (x3[i] - x3[j])*(x3[i] - x3[j]));
			float epot_ij = -m[i]*m[j]/dist;
			epot[i] += epot_ij;
			epot[j] += epot_ij;
		}
		#endif
	}
}

double grav_pot_unthreaded_no_avx(float *m, float *x1, float *x2, float *x3, float *epot, int n) {
	for (int i = 0; i < n; i++) {
		for (int j = i+1; j < n; j++) {
			float dist = sqrt((x1[i] - x1[j])*(x1[i] - x1[j]) + (x2[i] - x2[j])*(x2[i] - x2[j]) + (x3[i] - x3[j])*(x3[i] - x3[j]));
			float epot_ij = -m[i]*m[j]/dist;
			epot[i] += epot_ij;
			epot[j] += epot_ij;
		}

	}
}
