#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <math.h>

#include "../include/neighbour.h"


#if AVX
#include <immintrin.h>
#endif

#if OMP
#include <omp.h>
#endif

int _get_max_idx(float *data, int n) {
    int max_idx = 0;
    for (int i = 0; i < n; i++) {
        if (data[i] > data[max_idx]) {
            max_idx = i;
        }
    }
    return max_idx;
}

void _insertion_sort_dual_float_int(float* col1, int* col2, int n) {
        for (int i = 0; i < n; i++) {
            int cur_min_idx = i;
            for (int j = i; j < n; j++) {
                if (col1[j] < col1[cur_min_idx]) {
                    cur_min_idx = j;
                }
            }
            if (i != cur_min_idx) {
                int h_col2 = col2[i];
                float h_col1 = col1[i];
                col2[i] = col2[cur_min_idx];
                col1[i] = col1[cur_min_idx];
                col2[cur_min_idx] = h_col2;
                col1[cur_min_idx] = h_col1;
            }
        }
}

void _get_dist_list(float *X1, float *X2, float *X3, float X1_i, float X2_i, float X3_i, int n, float *dist_list) {
    #if AVX
    __m256* AVX_X1 = (__m256 *) X1;
    __m256* AVX_X2 = (__m256 *) X2;
    __m256* AVX_X3 = (__m256 *) X3;
    __m256 AVX_X1_i = _mm256_set1_ps(X1_i);
    __m256 AVX_X2_i = _mm256_set1_ps(X2_i);
    __m256 AVX_X3_i = _mm256_set1_ps(X3_i);
    __m256* AVX_DIST = (__m256 *) dist_list;

    for (int i = 0; i < (int) n/8 + 1; i++) {
        __m256 d_x1, d_x2, d_x3;
        
        d_x1 = _mm256_sub_ps(AVX_X1_i, AVX_X1[i]);
        d_x2 = _mm256_sub_ps(AVX_X2_i, AVX_X2[i]);
        d_x3 = _mm256_sub_ps(AVX_X3_i, AVX_X3[i]);

        d_x1 = _mm256_mul_ps(d_x1, d_x1);
        d_x2 = _mm256_mul_ps(d_x2, d_x2);
        d_x3 = _mm256_mul_ps(d_x3, d_x3);

        AVX_DIST[i] = _mm256_add_ps(d_x1, d_x2);
        AVX_DIST[i] = _mm256_add_ps(AVX_DIST[i], d_x3);
        //AVX_DIST[i] = _mm256_sqrt_ps(AVX_DIST[i]);
    
    }
    #else
    for (int j = 0; j < n; j++) {
        dist_list[j] = (X1_i - X1[j]) * (X1_i - X1[j]) \
                           + (X2_i - X2[j]) * (X2_i - X2[j]) \
                           + (X3_i - X3[j]) * (X3_i - X3[j]);
    }
    #endif
    
}

void get_neighbour_list(float *X1, float *X2, float *X3, int n, int nneigh, float **distance_list, int **index_list) {
    #if OMP
    #pragma omp parallel
    {
    #pragma omp for
    #endif    
    for (int i = 0; i < n; i++) {
        int current_max_idx = _get_max_idx(distance_list[i], nneigh + 1);
        float *local_dist_list = aligned_alloc(32,sizeof(float)*(n/8+1)*8);
        _get_dist_list(X1, X2, X3, X1[i], X2[i], X3[i], n, local_dist_list);

        if (n % 8 != 0) {
            for (int j = n; j < (n/8 + 1)*8; j++) {
                local_dist_list[j] = FLT_MAX;
            }
        }

        for (int j = 0; j < nneigh + 1; j++) {
            if (index_list[i][j] > -1) {
                distance_list[i][j] = (X2[i] - X1[index_list[i][j]]) * (X1[i] - X1[index_list[i][j]]) \
                                  + (X2[i] - X2[index_list[i][j]]) * (X2[i] - X2[index_list[i][j]]) \
                                  + (X3[i] - X3[index_list[i][j]]) * (X3[i] - X3[index_list[i][j]]);
            }
        }
        #if AVX
        __m256 AVX_CURRENT_MAX = _mm256_set1_ps(distance_list[i][current_max_idx]);
        __m256 *AVX_DIST = (__m256 *) local_dist_list;

        for (int j = 0; j < n/8 + 1; j++) {
            __m256 cmp = _mm256_cmp_ps(AVX_CURRENT_MAX, AVX_DIST[j], _CMP_GT_OQ);
            int mask = _mm256_movemask_ps(cmp);
            if (mask == 0x00) {
                continue;
            }
            for (int k = 0; k < 8; k++) {
                float d = AVX_DIST[j][k];
                if (d > distance_list[i][current_max_idx]) {
                    continue;
                } else {
                    if (j*8+k >= n) {
                        break;
                    }
                    distance_list[i][current_max_idx] = d;
                    index_list[i][current_max_idx] = j*8 + k;
                    current_max_idx = _get_max_idx(distance_list[i], nneigh + 1);
                    AVX_CURRENT_MAX = _mm256_set1_ps(distance_list[i][current_max_idx]);
                }
            }
        }
        #else
        for (int j = 0; j < n; j++) {
            float d = local_dist_list[j];
            if (d > distance_list[i][current_max_idx]) {
                continue;
            } else {
                distance_list[i][current_max_idx] = d;
                index_list[i][current_max_idx] = j;
                current_max_idx = _get_max_idx(distance_list[i], nneigh + 1);
            }
        }

        #endif
        _insertion_sort_dual_float_int(distance_list[i], index_list[i], nneigh + 1);
        free(local_dist_list);
    }
    #if OMP
    }
    #endif 
}

int calc_neighbour_density(float *M,
                            float *X1,
                            float *X2,
                            float *X3,
                            float *neighbour_density_n,
                            float *neighbour_density_m,
                            float **dist_list,
                            int **indx_list,
                            int n,
                            int nneigh
                            )
{
	int b_free_dist_and_index_list = 0;
	if ((dist_list == NULL && indx_list != NULL)
		 || (dist_list != NULL && indx_list == NULL)) {
		fprintf(stderr, "dist_list and idx_list must either both be NULL or not NULL\n");
		return -1;
	}
	if (dist_list == NULL && indx_list == NULL) {
		b_free_dist_and_index_list = 1;
		dist_list = malloc(sizeof(float*)*n);
		indx_list = malloc(sizeof(int*)*n);
		for (int i = 0; i < n; i++) {
			dist_list[i] = (float *) malloc(sizeof(float)*(nneigh + 1));
			indx_list[i] = (int *) malloc(sizeof(int)*(nneigh + 1));
			for (int j = 0; j < nneigh + 1; j++ ) {
				dist_list[i][j] = FLT_MAX;
				indx_list[i][j] = -1;
			} 
		}
       get_neighbour_list(X1, X2, X3, n, nneigh, dist_list, indx_list);
	}

    #if OMP
    #pragma omp parallel
    {
    #pragma omp for
    #endif    
	for (int i = 0; i < n; i++) {
		float avg_dist = 0;
		float avg_mass = 0;
		for (int j = 1; j < nneigh + 1; j++) {
			avg_dist += sqrt(dist_list[i][j])/nneigh;
			avg_mass += M[indx_list[i][j]]/nneigh;
		}
		neighbour_density_n[i] = 1./(4./3.*3.14159)*1./(avg_dist*avg_dist*avg_dist);
		neighbour_density_m[i] = 1./(4./3.*3.14159)*avg_mass/(avg_dist*avg_dist*avg_dist);
		if (neighbour_density_n[i] <= 0) {
			neighbour_density_n[i] = FLT_MIN;
		}
		if (neighbour_density_m[i] <= 0) {
			neighbour_density_m[i] = FLT_MIN;
		}
	}
	#if OMP
    }
    #endif	
	if (b_free_dist_and_index_list) {
		for (int i = 0; i < n; i++) {
			free(dist_list[i]);
			free(indx_list[i]);
		}
		free(dist_list);
		free(indx_list);
	}
    return 0;
}
int calc_neighbour_density_goodwin2004(float *M,
                            float *X1,
                            float *X2,
                            float *X3,
                            float *neighbour_density,
                            float **dist_list,
                            int **indx_list,
                            int n
                            )
{
    int nneigh = 5;
	int b_free_dist_and_index_list = 0;
	if ((dist_list == NULL && indx_list != NULL)
		 || (dist_list != NULL && indx_list == NULL)) {
		fprintf(stderr, "dist_list and idx_list must either both be NULL or not NULL\n");
		return -1;
	}
	if (dist_list == NULL && indx_list == NULL) {
		b_free_dist_and_index_list = 1;
		dist_list = malloc(sizeof(float*)*n);
		indx_list = malloc(sizeof(int*)*n);
		for (int i = 0; i < n; i++) {
			dist_list[i] = (float *) malloc(sizeof(float)*(nneigh + 1));
			indx_list[i] = (int *) malloc(sizeof(int)*(nneigh + 1));
			for (int j = 0; j < nneigh + 1; j++ ) {
				dist_list[i][j] = FLT_MAX;
				indx_list[i][j] = -1;
			} 
		}
       get_neighbour_list(X1, X2, X3, n, nneigh, dist_list, indx_list);
	}

    #if OMP
    #pragma omp parallel
    {
    #pragma omp for
    #endif    
	for (int i = 0; i < n; i++) {
		float dist = dist_list[i][nneigh];
		/*float mass = 0;
		for (int j = 1; j < nneigh + 1; j++) {
			mass += M[indx_list[i][j]];
		}*/
		neighbour_density[i] = M[i]*5./(4./3.*3.14159)*1./(sqrt(dist*dist*dist));
		if (neighbour_density[i] <= 0) {
			neighbour_density[i] = FLT_MIN;
		}
	}
	#if OMP
    }
    #endif	
	if (b_free_dist_and_index_list) {
		for (int i = 0; i < n; i++) {
			free(dist_list[i]);
			free(indx_list[i]);
		}
		free(dist_list);
		free(indx_list);
	}
}


void init_dist_indx_list(float **dist_list, int **indx_list, int n, int nneigh) {
	dist_list = init_dist_list(dist_list, n, nneigh);
	indx_list = init_indx_list(indx_list, n, nneigh);
}

float **init_dist_list(float **dist_list, int n, int nneigh) {
	dist_list = (float **) malloc(sizeof(float*)*n);
	for (int i = 0; i < n; i++) {
		dist_list[i] = (float *) malloc(sizeof(float)*(nneigh + 1));
		for (int j = 0; j < nneigh + 1; j++) {
			dist_list[i][j] = FLT_MAX;
		}
	}
	return dist_list;
}

int **init_indx_list(int **indx_list, int n, int nneigh) {
	indx_list = (int **) malloc(sizeof(int*)*n);
	for (int i = 0; i < n; i++) {
		indx_list[i] = (int *) malloc(sizeof(int)*(nneigh + 1));
		for (int j = 0; j < nneigh + 1; j++) {
			indx_list[i][j] = -1;
		}
	}
	return indx_list;
}

void reset_dist_indx_list(float **dist_list, int **indx_list, int n, int nneigh) {
	reset_dist_list(dist_list, n, nneigh);
	reset_indx_list(indx_list, n, nneigh);
}

void reset_dist_list(float **dist_list, int n, int nneigh) {
	for (int i = 0; i < n; i++) {
		dist_list[i] = (float *) malloc(sizeof(float)*(nneigh + 1));
		for (int j = 0; j < nneigh + 1; j++) {
			dist_list[i][j] = FLT_MAX;
		}
	}
}

void reset_indx_list(int **indx_list, int n, int nneigh) {
	for (int i = 0; i < n; i++) {
		indx_list[i] = (int *) malloc(sizeof(int)*(nneigh + 1));
		for (int j = 0; j < nneigh + 1; j++) {
			indx_list[i][j] = -1;
		}
	}
}

void free_dist_indx_list(float **dist_list, int **indx_list, int n) {
	free_dist_list(dist_list, n);
	free_indx_list(indx_list, n);
}

void free_dist_list(float **dist_list, int n) {
	for (int i = 0; i < n; i++) {
		free(dist_list[i]);
	}
	free(dist_list);
}

void free_indx_list(int **indx_list, int n) {
	for (int i = 0; i < n; i++) {
		free(indx_list[i]);
	}
	free(indx_list);
}
