__kernel void grav_pot_kernel(  __global float *m,
                                __global float *x1,
                                __global float *x2,
                                __global float *x3,
                                __global float *EPOT, 
                                int n)
{
    //Get our global thread ID
    int id = get_global_id(0);
    int i  = id;
    float EPOT_i = 0.0;
    //Make sure we do not go out of bounds
    if (id < n) {
       for (int j = 0; j < n; j++) {
		   if (i == j) continue;
           float dist = sqrt( (x1[i] - x1[j])*(x1[i] - x1[j]) + (x2[i] - x2[j])*(x2[i] - x2[j]) + (x3[i] - x3[j])*(x3[i] - x3[j]));
           EPOT_i +=  -m[i]*m[j]/dist;
       }
       EPOT[id] = EPOT_i;
    }
}
