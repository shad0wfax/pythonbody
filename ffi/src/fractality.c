#include <stdio.h>
#include <float.h>
#include <math.h>
#include <immintrin.h>
#include <../include/neighbour.h>

int count_boxes_with_stars(float *X1, float *X2, float *X3, float min, float max, int n_boxes_per_dim, int n) {
	int count = 0;
	//int tot = ;

	int *map = aligned_alloc(32, sizeof(int)*n_boxes_per_dim * n_boxes_per_dim * n_boxes_per_dim);
	
	#if AVX
	__m256i *AVX_MAP = (__m256i *) map;
	for (int i = 0; i < n_boxes_per_dim*n_boxes_per_dim*n_boxes_per_dim/8; i++){
		AVX_MAP[i] = _mm256_set1_epi32(0);
	}
	for (int i = (n_boxes_per_dim*n_boxes_per_dim*n_boxes_per_dim/8)*8; i < n_boxes_per_dim*n_boxes_per_dim*n_boxes_per_dim; i++) {
		map[i] = 0;
	}
	#else
	for (int i = 0; i < n_boxes_per_dim*n_boxes_per_dim*n_boxes_per_dim; i++){
		map[i] = 0;
	}
	#endif
	
	/*#if OMP
	#pragma omp parallel
	{
	#pragma omp for
	#endif*/
	for (int i = 0; i < n; i++) {
		int id_x = (X1[i] - min)/(max - min)*n_boxes_per_dim;
		int id_y = (X2[i] - min)/(max - min)*n_boxes_per_dim;
		int id_z = (X3[i] - min)/(max - min)*n_boxes_per_dim;
		int id_map = id_x*n_boxes_per_dim*n_boxes_per_dim + id_y*n_boxes_per_dim + id_z;
		if ((id_x < 0) || (id_y < 0) || (id_z < 0) || (id_map < 0)) {
			continue;
		} else if ((id_x >= n_boxes_per_dim) || id_y >= n_boxes_per_dim || id_z >= n_boxes_per_dim) {
			continue;
		}
		/*#if OMP
		#pragma omp critical 
		{
		#endif*/
		if (map[id_map] == 0) {
			count += 1;
			map[id_map] = 1;
		}
		/*#if OMP
		}
		#endif*/
	}
	/*#if OMP
	}
	#endif*/
	free(map);
	return count;
}

void get_goodwin2004_com(
		float com[3],
		float *M,
	    float *X1, 
		float *X2,
		float *X3,
		float *neighbour_density,
		int *n_densest_stars,
		int n)
{
	int b_free_n_densest_stars = 0;
	if (n_densest_stars == NULL) {
		n_densest_stars = (int *) malloc(sizeof(int));
		*n_densest_stars = 10;
		b_free_n_densest_stars = 1;
	}
	
	int idx_current_min = 0;
	com[0] = 0;
	com[1] = 0;
	com[2] = 0;

	int *indx_densest_stars = malloc(sizeof(int) * (*n_densest_stars));
	float *rho_densest_stars = malloc(sizeof(int) * (*n_densest_stars));
	for (int i = 0; i < *n_densest_stars; i++) {
		indx_densest_stars[i] = -1;
		rho_densest_stars[i] = FLT_MIN;
	}

	for (int i = 0; i < n; i++) {
		if (neighbour_density[i] > rho_densest_stars[idx_current_min]) {
			rho_densest_stars[idx_current_min] = neighbour_density[i];
			indx_densest_stars[idx_current_min] = i;

			for (int j = 0; j < *n_densest_stars; j++) {
				if (rho_densest_stars[j] < rho_densest_stars[idx_current_min]) {
					idx_current_min = j;
				}
			}
		}
	}

	float M_tot = 0.0;
	for (int i = 0; i < *n_densest_stars; i++) {
		M_tot += M[i];
		com[0] += M[i]*X1[indx_densest_stars[i]]; 
		com[1] += M[i]*X2[indx_densest_stars[i]]; 
		com[2] += M[i]*X3[indx_densest_stars[i]]; 
	}
	for (int i = 0; i < 3; i++) {
	   com[i] = com[i]/M_tot;
	}
		
	if (b_free_n_densest_stars) {
		free(n_densest_stars);
	}
	free(indx_densest_stars);
	free(rho_densest_stars);		
}

void adjust_goodwin_com(float com[3], float *X1, float *X2, float *X3, int n) {
	for (int i = 0; i < n; i++) {
		X1[i] -= com[0];
		X2[i] -= com[1];
		X3[i] -= com[2];
	}
}

void get_radius(float *X1, float *X2, float *X3, float *R, int n) {
	for (int i = 0; i < n; i++) {
		R[i] = sqrt(X1[i]*X1[i] + X2[i]*X2[i] + X3[i]*X3[i]);
	}
	
}

float *get_goodwin2004_fractality(
		float *goodwin_fractality,
		float *M,
		float *X1,
		float *X2,
		float *X3,
		float *neighbour_density,
		float **dist_list,
		int **indx_list,
		int *nneigh,
		int n)
{
	int b_free_dist_indx_list = 0;
	int b_free_nneigh = 0;
	if (nneigh == NULL) {
		nneigh = (int *) malloc(sizeof(int));
		*nneigh = 5;
		b_free_nneigh = 1;
	}
	if (dist_list == NULL && indx_list == NULL) {
		b_free_dist_indx_list = 1;
		dist_list = init_dist_list(NULL, n, *nneigh);
		indx_list = init_indx_list(NULL, n, *nneigh);

		get_neighbour_list(X1, X2, X3, n, *nneigh, dist_list, indx_list);
		calc_neighbour_density_goodwin2004(M, X1, X2, X3, neighbour_density, dist_list, indx_list, n);
	}
	float goodwin_com[3];
	float *R = malloc(sizeof(float)*n);
	int nshells = 100;
	float shell_average_rho[nshells];
	float total_average_rho = 0.0;
	int shell_f_count[nshells];
	int shell_n[nshells];
	float f_01 = 0;
	float f_05 = 0;
	float f_10 = 0;
	float f_20 = 0;
	float f_50 = 0;
	for (int i = 0; i < nshells; i++) {
		shell_average_rho[i] = 0;
		shell_f_count[i] = 0;
		shell_n[i] = 0;

	}

	get_goodwin2004_com(goodwin_com, M, X1, X2, X3, neighbour_density, NULL, n);
	adjust_goodwin_com(goodwin_com, X1, X2, X3, n);
	get_radius(X1, X2, X3, R, n);

	float max_r = 0.0;
	for (int i = 0; i < n; i++) {
		if (R[i] > max_r) {
			max_r = R[i];
		}
		total_average_rho += neighbour_density[i]/n;
	}
	for (int i = 0; i < n; i++) {
		float r = R[i]/max_r;
		R[i] = r;

		int shell_idx = (int) (r * nshells) - 1;
		shell_n[shell_idx] += 1;
		shell_average_rho[shell_idx] += neighbour_density[i]; 
	}
	for (int i = 0; i < nshells; i++) {
		if (shell_n[i] > 0) {
			shell_average_rho[i] = shell_average_rho[i]/shell_n[i];
		}
	}
		
	for (int i = 0; i < n; i++) {
		float r = R[i];

		int shell_idx = (int) (r * nshells) - 1;
		//if (neighbour_density[i] > 1.25 * shell_average_rho[shell_idx]) {
		if (neighbour_density[i] > 5*total_average_rho) {
			shell_f_count[shell_idx] += 1;
		}
	}

	int n_considered_shells = 0;
	for (int i = 0; i < nshells/2; i++) {
		if (shell_n[i] < 10) {
			continue;
		}
		if ((float) shell_f_count[i]/shell_n[i] >= 0.01) {
			f_01 += 1;
		}
		if ((float) shell_f_count[i]/shell_n[i] >= 0.05) {
			f_05 += 1;
		}
		if ((float) shell_f_count[i]/shell_n[i] >= 0.1) {
			f_10 += 1;
		}
		if ((float) shell_f_count[i]/shell_n[i] >= 0.2) {
			f_20 += 1;
		}
		if ((float) shell_f_count[i]/shell_n[i] >= 0.2) {
			f_20 += 1;
		}
		if ((float) shell_f_count[i]/shell_n[i] >= 0.5) {
			f_50 += 1;
		}
		n_considered_shells += 1;
	}
	f_01 = f_01/n_considered_shells;
	f_05 = f_05/n_considered_shells;
	f_10 = f_10/n_considered_shells;
	f_20 = f_20/n_considered_shells;
	f_50 = f_50/n_considered_shells;
	/*f_01 = f_01/nshells*2;
	f_05 = f_05/nshells*2;
	f_10 = f_10/nshells*2;
	f_20 = f_20/nshells*2;
	f_50 = f_50/nshells*2;*/
	
	#if DEBUG
	for (int i = 0; i < nshells; i++) {
		if (shell_n[i] >= 10) {
			printf("%d: N = %d RHO = %.03e F_COUNT = %d REL_F = %.02e\n", i, shell_n[i], shell_average_rho[i], shell_f_count[i], (float) shell_f_count[i]/shell_n[i]);
		}
	}
	printf("Goodwin COM: %.03e %.03e %.03e\n",goodwin_com[0], goodwin_com[1], goodwin_com[2]);

	printf("%d F01: %.02f F05: %.02f F10: %.02f F20: %.02f F_50: %.02f\n",n_considered_shells, f_01, f_05, f_10, f_20, f_50);
	#endif

	if (goodwin_fractality != NULL) {
		goodwin_fractality[0] = f_01;
		goodwin_fractality[1] = f_05;
		goodwin_fractality[2] = f_10;
		goodwin_fractality[3] = f_20;
		goodwin_fractality[4] = f_50;
	}

    for (int i = 0; i < 3; i++) {
        goodwin_com[i] = -goodwin_com[i];
    }
	adjust_goodwin_com(goodwin_com, X1, X2, X3, n);

	if (b_free_dist_indx_list) {
		free_dist_indx_list(dist_list, indx_list, n);
	}
	if (b_free_nneigh) {
		free(nneigh);
	}
	free(R);

	return goodwin_fractality;
}
