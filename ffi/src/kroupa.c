#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <math.h>

#define KROUPA_OFFSET1 2
#define KROUPA_OFFSET2 25
#define KROUPA_SCALE 0.125228288492496

float kroupa(float x) {	
	if (x < 0.08) {
		return pow(x,-0.3) * KROUPA_OFFSET2 * KROUPA_SCALE;
	} else if (x < 0.5) {
		return pow(x,-1.3) * KROUPA_OFFSET1 * KROUPA_SCALE;
	} else { 
		return pow(x,-2.3) * KROUPA_SCALE;
	}
}

void kroupa_n(float *dst, float *src, int n) {
	for (int i = 0; i < n; i++) {
		dst[i] = kroupa(src[i]);
	}
}

float kroupa_draw(float mmin, float mmax) {
	float ymin = kroupa(mmin);
	float ymax = kroupa(mmax);
	int N = 1000;
	float x_guess;
	float y_kroupa;
	float y_guess;


	while (1) {	
		x_guess = mmin + (mmax-mmin)*((float) rand()/RAND_MAX);
		y_guess = ymin + (ymax-ymin)*((float) rand()/RAND_MAX);
		y_kroupa = kroupa(x_guess);	

		if (y_guess < y_kroupa) {
			return x_guess;
		}		
	}
}

float *kroupa_draw_n(int n, float *dst, float mmin, float mmax) {
	if (dst == NULL) {
		dst = (float *) malloc(sizeof(float)*n);
	}

	for (int i = 0; i < n; i++) {
		dst[i] = kroupa_draw(mmin, mmax);
	}	

	return dst;
}
