#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <float.h>

#include "../include/ocl.h"
#include "../include/grav_pot.h"
#include "../include/cummean.h"
#include "../include/neighbour.h"
#include "../include/fractality.h"
#include "../include/shells.h"

#define N 100000
#define NNEIGH 5
#define SEED 314159

//int neighbour_density_omp_old(float *m, float *x1, float *x2, float *x3, float *neighbour_rho_n, float *neighbour_rho_m, int nneigh, int ntot, int* nprocs);	

void reset_epot(float* epot, int n) {
	for (int i = 0; i < n; i++) {
		epot[i] = 0;
	}
}

float get_wall_time() {
	struct timespec tw;
	clock_gettime(CLOCK_MONOTONIC, &tw);
	return tw.tv_sec*1000.0 + 1e-6*tw.tv_nsec;
}

int main (void) {
	/*float M[N];
	float X1[N];
	float X2[N];
	float X3[N];*/
	float *M = malloc(sizeof(float)*N);
	float *X1 = aligned_alloc(32,sizeof(float)*(N/8+1)*8);
	float *X2 = aligned_alloc(32,sizeof(float)*(N/8+1)*8);
	float *X3 = aligned_alloc(32,sizeof(float)*(N/8+1)*8);
    float **distance_list = init_dist_list(NULL, N, NNEIGH); //NULL; // = malloc(sizeof(float*)*N);
    int **index_list = init_indx_list(NULL, N, NNEIGH); //NULL; // = malloc(sizeof(int*)*N);
	float target[N];
    float neighbour_rho_n[N];
    float neighbour_rho_m[N];
    float neighbour_rho_goodwin2004[N];
	float EPOT_unthreaded[N];
	float EPOT_unthreaded_no_avx[N];
	float EPOT_unthreaded_avx[N];
	float EPOT_omp[N];
	float EPOT[N];
	clock_t t;
	float tw;
	/*float = malloc(sizeof(float*)*7);
	cols[0] = M;
	cols[1] = X1;
	cols[2] = X2;
	cols[3] = X3;
	cols[4] = neighbour_rho_n;
	cols[5] = neighbour_rho_m;
	cols[6] = EPOT;*/
	
	#if OCL
	printf("Initializing OpenCL ... ");
	ocl_init(0,0);
	//ocl_init_neighbour_density();
	ocl_init_grav_pot();
	ocl_init_cummean();
	printf("done\n");
	#endif
	srand(SEED);

	for (int i = 0; i < N; i++) {
		M[i] = (float) rand()/RAND_MAX;
		X1[i] = -1. + 2. * (float) rand()/RAND_MAX;
		X2[i] = -1. + 2. * (float) rand()/RAND_MAX;
		X3[i] = -1. + 2. *(float) rand()/RAND_MAX;
		EPOT_unthreaded_no_avx[i] = 0;
		EPOT_unthreaded_avx[i] = 0;
		EPOT_unthreaded[i] = 0;
		EPOT_omp[i] = 0;
        neighbour_rho_n[i] = 0;
        neighbour_rho_m[i] = 0;
	}
	
	printf("Testing grav_pot\n\n");
	
	tw = get_wall_time();
	t = clock();	
	grav_pot_unthreaded_no_avx(M,X1,X2,X3,EPOT_unthreaded_no_avx,N);
	t = clock() - t;
	tw = get_wall_time() - tw;
	printf("Unthreaded no AVX took %.03f s (CPU); %.03f ms (Wall)\n", (float) t/CLOCKS_PER_SEC, tw);
	for (int i = 0; i < 9; i++) {
	    printf("%.08e ", EPOT_unthreaded_no_avx[i]);
	}
	printf(" ... ");
	for (int i = 9; i > 0; i--) {
	    printf("%.08e ", EPOT_unthreaded_no_avx[N-i]);
	}
	printf("\n");

	tw = get_wall_time();
	t = clock();	
	grav_pot_unthreaded(M,X1,X2,X3,EPOT_unthreaded,N);
	t = clock() - t;
	tw = get_wall_time() - tw;
	printf("Unthreaded took %.03f s (CPU); %.03f ms (Wall)\n", (float) t/CLOCKS_PER_SEC, tw);
	for (int i = 0; i < 9; i++) {
	    printf("%.08e ", EPOT_unthreaded[i]);
	}
	printf(" ... ");
	for (int i = 9; i > 0; i--) {
	    printf("%.08e ", EPOT_unthreaded[N-i]);
	}
	printf("\n");
	//reset_epot(EPOT, N);
	
	tw = get_wall_time();
	t = clock();	
	grav_pot_omp(M,X1,X2,X3,EPOT_omp,N);
	t = clock() - t;
	tw = get_wall_time() - tw;
	printf("OpenMP took %.03f s (CPU); %.03f ms (Wall)\n", (float) t/CLOCKS_PER_SEC, tw);
	for (int i = 0; i < 9; i++) {
	    printf("%.08e ", EPOT_omp[i]);
	}
	printf(" ... ");
	for (int i = 9; i > 0; i--) {
	    printf("%.08e ", EPOT_omp[N-i]);
	}
	printf("\n");
	//reset_epot(EPOT, N);
	//
	for (int i = 0; i < N; i++) {
		if (EPOT_unthreaded[i] != EPOT_omp[i]) {
			printf("Diff at %d: %.05e\n",i, EPOT_unthreaded[i] - EPOT_omp[i]);
		}
	}
	
	#if OCL
	tw = get_wall_time();
	t = clock();
	grav_pot_ocl(M,X1,X2,X3,EPOT,N);
	t = clock() - t;
	tw = get_wall_time() - tw;
	printf("OpenCL took %.03f s (CPU); %.03f ms (Wall)\n", (float) t/CLOCKS_PER_SEC, tw);
	for (int i = 0; i < 9; i++) {
	    printf("%03.02f ", EPOT[i]);
	}
	printf(" ... ");
	for (int i = 9; i > 0; i--) {
	    printf("%03.02f ", EPOT[N-i]);
	}
	printf("\n");
	reset_epot(EPOT, N);

	ocl_free_grav_pot();
	ocl_free_cummean();
	ocl_free();
	#endif

	printf("\n\n");
	printf("Testing neigbour list\n\n");
	
    // actual program
	tw = get_wall_time();
	t = clock();
    get_neighbour_list(X1, X2, X3, N, NNEIGH, distance_list, index_list);
	t = clock() - t;
	tw = get_wall_time() - tw;
	printf("OMP_AVX %.03f s (CPU); %.03f ms (Wall)\n", (float) t/CLOCKS_PER_SEC, tw);
	for (int i = 0; i < NNEIGH + 1; i++) {
	    printf("%.02e ", distance_list[0][i]);
	}
	printf(" ... ");
	for (int i = 0; i < NNEIGH + 1; i++) {
	    printf("%02e ", distance_list[N-1][i]);
	}
	printf("\n");
	for (int i = 0; i < NNEIGH + 1; i++) {
	    printf("%d ", index_list[0][i]);
	}
	printf(" ... ");
	for (int i = 0; i < NNEIGH + 1; i++) {
	    printf("%d ", index_list[N-1][i]);
	}
	printf("\n");
	
	printf("\n\n");
	printf("Testing neigbour density\n\n");
	tw = get_wall_time();
	t = clock();
    calc_neighbour_density(M, X1, X2, X3, neighbour_rho_n, neighbour_rho_m, distance_list, index_list, N, NNEIGH);
	t = clock() - t;
	tw = get_wall_time() - tw;
	printf("Neighbour_density %.03f s (CPU); %.03f ms (Wall)\n", (float) t/CLOCKS_PER_SEC, tw);
	for (int i = 0; i < 9; i++) {
	    printf("%03.02f ", neighbour_rho_n[i]);
	}
	printf(" ... ");
	for (int i = 9; i > 0; i--) {
	    printf("%03.02f ", neighbour_rho_n[N-i]);
	}
	printf("\n");
	for (int i = 0; i < 9; i++) {
	    printf("%03.02f ", neighbour_rho_m[i]);
	}
	printf(" ... ");
	for (int i = 9; i > 0; i--) {
	    printf("%03.02f ", neighbour_rho_m[N-i]);
	}
	printf("\n");

	for (int i = 0; i < N; i++) {
		neighbour_rho_n[i] = 0;
		neighbour_rho_m[i] = 0;
	}
	
	tw = get_wall_time();
	t = clock();
    calc_neighbour_density(M, X1, X2, X3, neighbour_rho_n, neighbour_rho_m, NULL, NULL, N, NNEIGH);
	t = clock() - t;
	tw = get_wall_time() - tw;
	printf("Neighbour_density w/o dist & indx list %.03f s (CPU); %.03f ms (Wall)\n", (float) t/CLOCKS_PER_SEC, tw);
	for (int i = 0; i < 9; i++) {
	    printf("%03.02f ", neighbour_rho_n[i]);
	}
	printf(" ... ");
	for (int i = 9; i > 0; i--) {
	    printf("%03.02f ", neighbour_rho_n[N-i]);
	}
	printf("\n");
	for (int i = 0; i < 9; i++) {
	    printf("%03.02f ", neighbour_rho_m[i]);
	}
	printf(" ... ");
	for (int i = 9; i > 0; i--) {
	    printf("%03.02f ", neighbour_rho_m[N-i]);
	}
	printf("\n");

	for (int i = 0; i < N; i++) {
		neighbour_rho_n[i] = 0;
		neighbour_rho_m[i] = 0;
	}
	
    tw = get_wall_time();
	t = clock();
    calc_neighbour_density_goodwin2004(M, X1, X2, X3, neighbour_rho_goodwin2004, NULL, NULL, N);
	t = clock() - t;
	tw = get_wall_time() - tw;
	printf("neighbour_density_goodwin2004 w/o dist & indx list %.03f s (CPU); %.03f ms (Wall)\n", (float) t/CLOCKS_PER_SEC, tw);
	for (int i = 0; i < 9; i++) {
	    printf("%03.02f ", neighbour_rho_goodwin2004[i]);
	}
	printf(" ... ");
	for (int i = 9; i > 0; i--) {
	    printf("%03.02f ", neighbour_rho_goodwin2004[N-i]);
	}
    printf("\n");
   
   	float goodwin_fractality[5];	
	tw = get_wall_time();
	t = clock();
    get_goodwin2004_fractality(goodwin_fractality, M, X1, X2, X3, neighbour_rho_goodwin2004, NULL, NULL,NULL, N);
	t = clock() - t;
	tw = get_wall_time() - tw;
	printf("neighbour_density_goodwin2004 w/o dist & indx list %.03f s (CPU); %.03f ms (Wall)\n", (float) t/CLOCKS_PER_SEC, tw);
	printf("F_01, F_05, F_10, F_20, F_50 = ");
	for (int i = 0; i < 5; i++) {
		printf("%.03f ", goodwin_fractality[i]);
	}
	printf("\n");
	/*for (int i = 0; i < 9; i++) {
	    printf("%03.02f ", neighbour_rho_goodwin2004[i]);
	}
	printf(" ... ");
	for (int i = 9; i > 0; i--) {
	    printf("%03.02f ", neighbour_rho_goodwin2004[N-i]);
	}
    printf("\n");*/

    // print output
    /*for (int i = 0; i < N; i++) {
        printf("%d:", i);
        for (int j = 0; j < NNEIGH+1; j++) {
            printf(" (%d %f)", index_list[i][j], distance_list[i][j]);
        }
        printf("\n");
    }*/
    tw = get_wall_time();
	t = clock();
    float count = count_boxes_with_stars(X1, X2, X3, -1., 1., 100, N);
	t = clock() - t;
	tw = get_wall_time() - tw;
	printf("Counting boxes took %.03f s (CPU); %.03f ms (Wall)\n", (float) t/CLOCKS_PER_SEC, tw);
    printf("Box_count: %f\n", count);

	/*printf("\n\nTesting shells\n\n");
    tw = get_wall_time();
	t = clock();
    calc_shells(M, cols);
	t = clock() - t;
	tw = get_wall_time() - tw;
	printf("Counting boxes took %.03f s (CPU); %.03f ms (Wall)\n", (float) t/CLOCKS_PER_SEC, tw);*/


	free_dist_indx_list(distance_list, index_list, N);
	//free(cols);
	
    free(M);
    free(X1);
    free(X2);
    free(X3);	
	return 0;
}
