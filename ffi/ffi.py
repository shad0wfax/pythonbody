from ctypes import cdll, c_int, c_uint, c_float, c_void_p, byref, POINTER
import os
import numpy as np
import pandas as pd
import pathlib
import re
import datetime as dt


class FFI:
    """
    Class for handling calls to the C foreign function interface.
    A default instance of class can be found at ``pythonbody.ffi.ffi``.
    The Class can be found at ``pythonbody.ffi.FFI``.

    .. code-block:: python

        from pythonbody.ffi import ffi # default instance
        from pythonbody.ffi import FFI # class
    """
    def __init__(self, p_id: int = None, d_id: int = None):
        cwd = os.path.realpath(__file__)
        cwd = cwd[:cwd.rfind("/") + 1]
        if pathlib.Path("pythonbody/ffi/.libs/libpythonbody.so").is_file():
            self.lib = cdll.LoadLibrary(
                    "pythonbody/ffi/.libs/libpythonbody.so"
                    )
        elif pathlib.Path("ffi/.libs/libpythonbody.so").is_file():
            self.lib = cdll.LoadLibrary("ffi/.libs/libpythonbody.so")
        elif pathlib.Path(".libs/libpythonbody.so").is_file():
            self.lib = cdll.LoadLibrary(".libs/libpythonbody.so")
        elif pathlib.Path(cwd + ".libs/libpythonbody.so").is_file():
            self.lib = cdll.LoadLibrary(cwd + ".libs/libpythonbody.so")
        else:
            print("FFI: Couldn't load libpythonbody.so, FFI will not be working!")
            self.OpenCL = False
            return None
        try:
            self._ocl_init(p_id, d_id)
            self.default_cfunc = "ocl"
            self.OpenCL = True
        except:
            print("FFI: Couldn't load OpenCL, defaulting back to OpenMP")
            self.default_cfunc = "omp"
            self.OpenCL = False
        

    def __del__(self):
        if self.OpenCL:
            self._ocl_free_grav_pot()
            self._ocl_free_cummean()
            self._ocl_free()

    def _ocl_init(self, p_id: int = None, d_id: int = None):
        func = self.lib.ocl_init
        func.argtypes = [c_void_p, c_void_p]
        func.restype = c_int

        err = func(c_void_p(None) if p_id is None else byref(c_int(p_id)),
                   c_void_p(None) if d_id is None else byref(c_int(d_id)))
        if err != 0:
            raise Exception("Error initialize OpenCL")
        self._ocl_init_cummean()
        self._ocl_init_grav_pot()
        self._ocl_init_neighbour_density()

    def _ocl_init_cummean(self):
        func = self.lib.ocl_init_cummean
        func.restype = c_int

        err = func()
        if err != 0:
            raise Exception("Error initialize OpenCL")
    
    def _ocl_init_grav_pot(self):
        func = self.lib.ocl_init_grav_pot
        func.restype = c_int

        err = func()
        if err != 0:
            raise Exception("Error initialize OpenCL")
    
    def _ocl_init_neighbour_density(self):
        func = self.lib.ocl_init_neighbour_density
        func.restype = c_int

        err = func()
        if err != 0:
            raise Exception("Error initialize OpenCL")

    def _ocl_free_grav_pot(self):
        func = self.lib.ocl_free_grav_pot
        func()
    def _ocl_free_cummean(self):
        func = self.lib.ocl_free_cummean
        func()
    def _ocl_free(self):
        func = self.lib.ocl_free
        func()

    def cummean(self, data: np.array, c_func="auto"):
        if c_func not in ["ocl","omp","unthreaded","auto", None]:
            raise ValueError("c_func must be either cuda, ocl, omp, unthreaded or None")
        if c_func == "auto":
            c_func = self.default_cfunc
        N = data.shape[0]
        target = (c_float * N)(*np.zeros(N))
        if c_func is not None:
            func = eval(f"self.lib.cummean_{c_func}")
        else:
            func = eval(f"self.lib.cummean")
        func.argtypes = [ 
                c_float * N, # target
                c_float * N, # source
                c_int,        # N 
                ]   
        func.restype = c_int 

        func(target,
                (c_float * N)(*data),
                N,  
            )   
        return np.array(target)
    def grav_pot(self,
                 data: pd.DataFrame,
                 G: float = 1,
                 num_threads: int = None,
                 c_func ="auto"
                 ):
        if c_func not in ["ocl","omp","unthreaded","auto", None]:
            raise ValueError("c_func must be either cuda, ocl, omp, unthreaded or None")
        if c_func == "auto":
            c_func = self.default_cfunc

        N = data.shape[0]

        EPOT = (c_float * N)(*np.zeros(N))
        if c_func is not None:
            func = eval(f"self.lib.grav_pot_{c_func}")
        else:
            func = eval(f"self.lib.grav_pot")
        
        func.argtypes = [
                c_float * N, # M
                c_float * N, # X1 
                c_float * N, # X2
                c_float * N, # X3
                c_float * N, # EPOT (results)
                c_int,        # N
                ]#c_int,        # num_threads
                #]
        func.restype = c_int 

        func((c_float * N)(*data["M"].values),
                    (c_float * N)(*data["X1"].values),
                    (c_float * N)(*data["X2"].values),
                    (c_float * N)(*data["X3"].values),
                    EPOT,
                    N,
                    num_threads
                    )
        return np.array(EPOT, dtype=np.float32)

    def calc_shells(self,
                    data: pd.DataFrame,
                    by: str = "M",
                    n_shells: int = 100,
                    shells: np.array = np.array([ 0, 0.001, 0.003, 0.005, 0.01, 0.03,
                                                  0.05, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6,
                                                  0.7, 0.8, 0.9, 0.95, 0.99, 1.0], dtype=np.float32),
                    method: str = "mean",
                    std: bool = False,
                    ):

        """
        gets cell data
        """

        if method not in ["sum", "mean", "cumsum", "cummean"]:
            raise ValueError(f"Method must be 'sum', 'mean', 'cumsum' or 'cummean', but is '{method}'")
        if by == "M" or by == "M/MT":
            by = "M/MT_cum"
            if by not in data.columns:
                data.calc_M_over_MT_cum()

        if by not in data.columns:
            raise KeyError(f"Cannot get shells by {by}, as {by} is not in columns: {data.columns}")
        if shells.dtype != np.float32:
            shells = np.array(shells, dtype=np.float32)

        if not re.search(".*/.*T(_cum)?", by) and f"{by}/{by}T" not in data.columns:
            try: 
                eval(f"data.calc_{by}_over_{by}T()")
            except:
                try: 
                    data[f"{by}/{by}T"] = data[by].cumsum()/data[by].sum()
                except:
                    raise KeyError(f"Couldn't calculate {by}/{by}T")
        
        if not re.search(".*/.*T(_cum)?", by):
            by = f"{by}/{by}T"

        if n_shells is not None:
            shells = np.linspace(data[by].min(),data[by].max(),n_shells+1, dtype=np.float32)

        data = data[(data.dtypes == np.float32) | (data.dtypes == np.float64)].astype(np.float32)
        func = eval(f"self.lib.calc_shells")
        N = data.shape[0]
        NCOLS = data.shape[1]
        NSHELLBOUNDS = len(shells)

        shell_data = np.zeros((NSHELLBOUNDS - 1) * NCOLS, dtype=np.float32)
        shell_particle_count = np.zeros(NSHELLBOUNDS - 1, dtype = np.int32);
        cols = data.values.flatten()
        shell_data_std = None
        shell_data_std_mask = np.zeros(NCOLS, dtype=bool)
        if std:
            shell_data_std = np.zeros((NSHELLBOUNDS - 1)*NCOLS, dtype=np.float32)
        
        func.argtypes = [
                POINTER(c_float),     # primary col 
                POINTER(c_float),     # columns
                POINTER(c_float),      # shell boundaries
                POINTER(c_float), # shell data
                POINTER(c_float), # shell data std
                POINTER(c_int),   # shell particle count
                c_int,         # NTOT
                c_int,         # nshellbounds
                c_int,         # ncols
        ]
        arg1 = data[by].values.ctypes.data_as(POINTER(c_float))
        arg2 = cols.ctypes.data_as(POINTER(c_float))
        arg3 = shells.ctypes.data_as(POINTER(c_float))
        arg4 = shell_data.ctypes.data_as(POINTER(c_float))
        arg5 = shell_data_std.ctypes.data_as(POINTER(c_float)) if std else POINTER(c_float)()
        arg6 = shell_particle_count.ctypes.data_as(POINTER(c_int))
        arg7 = N
        arg8 = NSHELLBOUNDS
        arg9 = NCOLS              
        func(arg1, arg2, arg3, arg4, arg5, arg6, arg7, arg8, arg9)

        output_columns = data.columns
        shell_data = shell_data.reshape(NSHELLBOUNDS - 1, NCOLS)
        if std:
            shell_data_std = shell_data_std.reshape(NSHELLBOUNDS - 1, NCOLS)
            output_columns = list(output_columns) + [f"{col}_std" for col in output_columns]
            shell_data_std = np.sqrt(shell_data_std)
            shell_data = np.concatenate((shell_data, shell_data_std), axis=1)
            shell_data_std_mask = np.concatenate((shell_data_std_mask, np.ones(len(shell_data_std_mask), dtype=bool)), dtype=bool)

        shell_df = pd.DataFrame(data=shell_data,
                                index=shells[1:],
                                columns=output_columns)

        if method == "mean":
            shell_df.iloc[:,~shell_data_std_mask] = (shell_df.iloc[:,~shell_data_std_mask].T/shell_particle_count).T
        elif method == "cumsum":
            shell_df.iloc[:,~shell_data_std_mask] = shell_df.iloc[:,~shell_data_std_mask].cumsum()
            shell_particle_count = shell_particle_count.cumsum()
        elif method == "cummean":
            shell_particle_count = shell_particle_count.cumsum()
            shell_df.iloc[:,~shell_data_std_mask] = shell_df.iloc[:,~shell_data_std_mask].cumsum()
            shell_df.iloc[:,~shell_data_std_mask] = (shell_df.iloc[:,~shell_data_std_mask].T/shell_particle_count).T

        
        shell_df["N"] = shell_particle_count
        
        return shell_df
    
    def calc_cells2d(self,
                    data: pd.DataFrame,
                    colA: str = "X1",
                    colB: str = "X2",
                    dim_min: float = None,
                    dim_max: float = None,
                    dim_minA: float = None,
                    dim_maxA: float = None,
                    dim_minB: float = None,
                    dim_maxB: float = None,
                    n_cells_per_dim: int = 100,
                    method: str = "mean",
                    std: bool = False,
                    ):
        """
        gets two-dimensional cell data
        """

        if colA not in data.columns:
            raise ValueError(f"Couldn't find column '{colA}' in data")
        if colB not in data.columns:
            raise ValueError(f"Couldn't find column '{colA}' in data")

        if method not in ["sum", "mean"]:
            raise ValueError(f"Method must be 'sum', 'mean', but is '{method}'")

        if dim_min is not None:
            if dim_minA is None:
                dim_minA = dim_min
            if dim_minB is None:
                dim_minB = dim_min
        if dim_max is not None:
            if dim_maxA is None:
                dim_maxA = dim_max
            if dim_maxB is None:
                dim_maxB = dim_max

        b_std = std
        if dim_min is None and dim_minA is None and dim_minB is None:
            std = data[[colA, colB]].std().mean()
            dim_min = dim_minA = dim_minB = -3*std
        if dim_max is None and dim_maxA is None and dim_maxB is None:
            std = data[[colA, colB]].std().mean()
            dim_max = dim_maxA = dim_maxB = 3*std
        std = b_std

        data = data[(data.dtypes == np.float32) | (data.dtypes == np.float64)].astype(np.float32)
        func = eval(f"self.lib.calc_cells2d")
        N = data.shape[0]
        NCOLS = data.shape[1]

        cell_data = np.zeros(n_cells_per_dim * n_cells_per_dim * NCOLS, dtype=np.float32)
        cell_particle_count = np.zeros(n_cells_per_dim*n_cells_per_dim, dtype = np.int32);
        cols = data.values.flatten()
        cell_data_std = None
        cell_data_std_mask = np.zeros(NCOLS, dtype=bool)
        if std:
            cell_data_std = np.zeros(n_cells_per_dim*n_cells_per_dim*NCOLS, dtype=np.float32)
        
        func.argtypes = [
                POINTER(c_float),     # colA 
                POINTER(c_float),     # colB 
                POINTER(c_float),     # columns
                POINTER(c_float), # cell data
                POINTER(c_float), # cell data std
                POINTER(c_int),   # cell particle count
                c_float,       # dimA min
                c_float,       # dimA max
                c_float,       # dimB min
                c_float,       # dimB max
                c_int,         # NTOT
                c_int,         # ncells (per dim)
                c_int,         # ncols
        ]
        args = [
                data[colA].values.ctypes.data_as(POINTER(c_float)),
                data[colB].values.ctypes.data_as(POINTER(c_float)),
                cols.ctypes.data_as(POINTER(c_float)),
                cell_data.ctypes.data_as(POINTER(c_float)),
                cell_data_std.ctypes.data_as(POINTER(c_float)) if std else POINTER(c_float)(),
                cell_particle_count.ctypes.data_as(POINTER(c_int)),
                dim_minA,
                dim_maxA,
                dim_minB,
                dim_maxB,
                N,
                n_cells_per_dim,
                NCOLS,
                ]
        func(*args)

        cell_data = cell_data.reshape(n_cells_per_dim, n_cells_per_dim, NCOLS)
        cell_particle_count = cell_particle_count.reshape(n_cells_per_dim, n_cells_per_dim)

        output_columns = data.columns

        if method == "mean":
            mask = cell_particle_count > 0
            cell_data[mask,:] = ((cell_data[mask,:]).T/cell_particle_count[mask]).T

        mask = cell_particle_count == 0
        for i in range(0, cell_data.shape[2]):
            cell_data[:,:,i][mask] = np.nan

        
        if std:
            cell_data_std = cell_data_std.reshape(n_cells_per_dim, n_cells_per_dim, NCOLS)
            output_columns = list(output_columns) + [f"{col}_std" for col in output_columns]
            cell_data_std = np.sqrt(cell_data_std)
            cell_data = np.concatenate((cell_data, cell_data_std), axis=2)
        
        out_data = {col: cell_data[:,:,i] for i,col in enumerate(output_columns)}
        out_data["colA"] = np.linspace(dim_minA, dim_maxA, n_cells_per_dim)
        out_data["colB"] = np.linspace(dim_minB, dim_maxB, n_cells_per_dim)
        out_data["N"] = cell_particle_count
        return out_data        
    
    def calc_cells3d(self,
                    data: pd.DataFrame,
                    colA: str = "X1",
                    colB: str = "X2",
                    colC: str = "X3",
                    dim_min: float = None,
                    dim_max: float = None,
                    dim_minA: float = None,
                    dim_maxA: float = None,
                    dim_minB: float = None,
                    dim_maxB: float = None,
                    dim_minC: float = None,
                    dim_maxC: float = None,
                    n_cells_per_dim: int = 50,
                    method: str = "mean",
                    std: bool = False,
                    ):

        """
        gets three-dimensional cell data
        """

        if colA not in data.columns:
            raise ValueError(f"Couldn't find column '{colA}' in data")
        if colB not in data.columns:
            raise ValueError(f"Couldn't find column '{colB}' in data")
        if colC not in data.columns:
            raise ValueError(f"Couldn't find column '{colC}' in data")

        if method not in ["sum", "mean"]:
            raise ValueError(f"Method must be 'sum', 'mean', but is '{method}'")

        if dim_min is not None:
            if dim_minA is None:
                dim_minA = dim_min
            if dim_minB is None:
                dim_minB = dim_min
            if dim_minC is None:
                dim_minC = dim_min
        if dim_max is not None:
            if dim_maxA is None:
                dim_maxA = dim_max
            if dim_maxB is None:
                dim_maxB = dim_max
            if dim_maxC is None:
                dim_maxC = dim_max

        b_std = std
        if dim_min is None and dim_minA is None and dim_minB is None:
            std = data[[colA, colB, colC]].std().mean()
            dim_min = dim_minA = dim_minB = dim_minC = -3*std
        if dim_max is None and dim_maxA is None and dim_maxB is None:
            std = data[[colA, colB, colC]].std().mean()
            dim_max = dim_maxA = dim_maxB = dim_maxC = 3*std
        std = b_std

        data = data[(data.dtypes == np.float32) | (data.dtypes == np.float64)].astype(np.float32)
        func = eval(f"self.lib.calc_cells3d")
        N = data.shape[0]
        NCOLS = data.shape[1]

        cell_data = np.zeros(n_cells_per_dim*n_cells_per_dim * n_cells_per_dim * NCOLS, dtype=np.float32)
        cell_particle_count = np.zeros(n_cells_per_dim*n_cells_per_dim*n_cells_per_dim, dtype = np.int32);
        cols = data.values.flatten()
        cell_data_std = None
        cell_data_std_mask = np.zeros(NCOLS, dtype=bool)
        if std:
            cell_data_std = np.zeros(n_cells_per_dim*n_cells_per_dim*n_cells_per_dim*NCOLS, dtype=np.float32)
        
        func.argtypes = [
                POINTER(c_float),     # colA 
                POINTER(c_float),     # colB 
                POINTER(c_float),     # colC 
                POINTER(c_float),     # columns
                POINTER(c_float), # cell data
                POINTER(c_float), # cell data std
                POINTER(c_int),   # cell particle count
                c_float,       # dimA min
                c_float,       # dimA max
                c_float,       # dimB min
                c_float,       # dimB max
                c_float,       # dimC min
                c_float,       # dimC max
                c_int,         # NTOT
                c_int,         # ncells (per dim)
                c_int,         # ncols
        ]
        args = [
                data[colA].values.ctypes.data_as(POINTER(c_float)),
                data[colB].values.ctypes.data_as(POINTER(c_float)),
                data[colC].values.ctypes.data_as(POINTER(c_float)),
                cols.ctypes.data_as(POINTER(c_float)),
                cell_data.ctypes.data_as(POINTER(c_float)),
                cell_data_std.ctypes.data_as(POINTER(c_float)) if std else POINTER(c_float)(),
                cell_particle_count.ctypes.data_as(POINTER(c_int)),
                dim_minA,
                dim_maxA,
                dim_minB,
                dim_maxB,
                dim_minC,
                dim_maxC,
                N,
                n_cells_per_dim,
                NCOLS,
                ]
        func(*args)

        cell_data = cell_data.reshape(n_cells_per_dim, n_cells_per_dim, n_cells_per_dim , NCOLS)
        cell_particle_count = cell_particle_count.reshape(n_cells_per_dim, n_cells_per_dim, n_cells_per_dim)

        output_columns = data.columns

        if method == "mean":
            mask = cell_particle_count > 0
            cell_data[mask,:] = ((cell_data[mask,:]).T/cell_particle_count[mask]).T
        
        mask = cell_particle_count == 0
        for i in range(0, cell_data.shape[3]):
            cell_data[:,:,:,i][mask] = np.nan
        
        if std:
            cell_data_std = cell_data_std.reshape(n_cells_per_dim, n_cells_per_dim, n_cells_per_dim, NCOLS)
            output_columns = list(output_columns) + [f"{col}_std" for col in output_columns]
            cell_data_std = np.sqrt(cell_data_std)
            cell_data = np.concatenate((cell_data, cell_data_std), axis=2)
        
        out_data = {col: cell_data[:,:,:,i] for i,col in enumerate(output_columns)}
        out_data["colA"] = np.linspace(dim_minA, dim_maxA, n_cells_per_dim)
        out_data["colB"] = np.linspace(dim_minB, dim_maxB, n_cells_per_dim)
        out_data["colC"] = np.linspace(dim_minC, dim_maxC, n_cells_per_dim)
        out_data["N"] = cell_particle_count
        return out_data        

    def neighbour_density(self,
                          data: pd.DataFrame,
                          num_threads: int = None,
                          c_func: str = "omp",
                          n_neigh: int = 80,
                          omp_n_procs: int = None,
                          input_columns: list = ["M", "X1", "X2", "X3"]
                          #dist_indx_lst: bool = False
                          ):

        if c_func == "ocl" and n_neigh != 80:
            raise ValueError("OpenCL only allows n_neigh to be equal to 80!")

        if c_func is None or c_func.lower() == "auto":
            c_func = "omp"
        if omp_n_procs is not None:
            c_func = "omp"

        func = eval(f"self.lib.calc_neighbour_density")
        N = data.shape[0]
        rho_n = (c_float * N) (*np.zeros(N))
        rho_m = (c_float * N) (*np.zeros(N))

        """if dist_indx_lst:
            dist_list = (c_float * (N * n_neigh)) (*np.zeros((N, n_neigh)))
            indx_list = (c_float * (N * n_neigh)) (*np.zeros((N, n_neigh)))
        else:"""
        dist_list = POINTER(POINTER(c_float))()
        indx_list = POINTER(POINTER(c_int))()


        func.argtypes = [
                c_float * N,  # M 
                c_float * N,  # X1
                c_float * N,  # X2
                c_float * N,  # X3
                c_float * N,  # rho_n
                c_float * N,  # rho_m
                c_void_p,     # dist_list
                c_void_p,     # indx_list
                c_int,        # N_TOT
                c_int,        # N_NEIGH,
                ]
        func.restype = c_int

        func( (c_float * N) (*data[input_columns[0]].values),
              (c_float * N) (*data[input_columns[1]].values),
              (c_float * N) (*data[input_columns[2]].values),
              (c_float * N) (*data[input_columns[3]].values),
              rho_n,
              rho_m,
              dist_list,
              indx_list,
              N,
              n_neigh)
        return np.array((rho_n, rho_m))

    def neighbour_density_goodwin2004(self,
                                      data: pd.DataFrame):
        func = eval(f"self.lib.calc_neighbour_density_goodwin2004")
        N = data.shape[0]
        rho = (c_float * N) (*np.zeros(N))

        func.argtypes = [
                c_float * N,  # M 
                c_float * N,  # X1
                c_float * N,  # X2
                c_float * N,  # X3
                c_float * N,  # neighbour_density
                c_void_p,     # dist_list
                c_void_p,     # indx_list
                c_int,        # N_TOT
                ]
        func.restype = c_int

        func( (c_float * N) (*data["M"].values),
              (c_float * N) (*data["X1"].values),
              (c_float * N) (*data["X2"].values),
              (c_float * N) (*data["X3"].values),
              rho,
              POINTER(POINTER(c_float))(),
              POINTER(POINTER(c_int))(),
              N)
        return np.array(rho)

    def goodwin_fractality(self,
                           data: pd.DataFrame,
                           ):

        func = eval(f"self.lib.get_goodwin2004_fractality")
        N = data.shape[0]
        fractalities = (c_float * 5) (*np.zeros(5))
        rho_n = (c_float * N) (*np.zeros(N))

        func.argtypes = [
                c_float * 5,  # fractalities
                c_float * N,  # M 
                c_float * N,  # X1
                c_float * N,  # X2
                c_float * N,  # X3
                c_float * N,  # neighbour_density
                c_void_p,     # dist_list
                c_void_p,     # indx_list
                c_void_p,     # N_NEIGH,
                c_int,        # N_TOT
                ]
        func.restype = POINTER(c_float)

        func( fractalities,
              (c_float * N) (*data["M"].values),
              (c_float * N) (*data["X1"].values),
              (c_float * N) (*data["X2"].values),
              (c_float * N) (*data["X3"].values),
              rho_n,
              POINTER(POINTER(c_float))(),
              POINTER(POINTER(c_int))(),
              POINTER(c_int)(),
              N
              )
        return np.array(fractalities)

    def box_count(self,
                  data: pd.DataFrame,
                  dim_min: float = None,
                  dim_max: float = None,
                  n_boxes_per_dim: int = 10,
                  ):

        func = eval(f"self.lib.count_boxes_with_stars")
        N = data.shape[0]

        if dim_min is None:
            dim_min = -3*data[["X1", "X2", "X3"]].std().mean()
        if dim_max is None:
            dim_max = 3*data[["X1", "X2", "X3"]].std().mean()

        func.argtypes = [
                c_float * N,  # X1
                c_float * N,  # X2
                c_float * N,  # X3
                c_float,      # min
                c_float,      # max
                c_int,        # N_boxes per dim
                c_int,        # N_TOT
                ]
        func.restype = c_int

        res = func( 
              (c_float * N) (*data["X1"].values),
              (c_float * N) (*data["X2"].values),
              (c_float * N) (*data["X3"].values),
              dim_min,
              dim_max,
              n_boxes_per_dim,
              N
              )
        return res
    def kroupa_draw(self,
                    n: int = 1,
                    mmin: float = 0.08,
                    mmax: float = 120):
        """
        Draw mass(es) from kroupa IMF

        :param n: number of masses to draw
        :type n: int
        :param mmin: Minimum mass from IMF
        :param mmax: Maximum mass from IMF
        :returns: Mass or array of Masses
        :rtype: float, np.array
        """
        assert n > 0
        
        if n == 1:
            return self._kroupa_draw(mmin, mmax)
        else:
            return self._kroupa_draw_n(n, mmin, mmax)

    def _kroupa_draw(self, mmin: float, mmax: float):
        func = self.lib.kroupa_draw
        func.restype = c_float
        func.argtypes = [
                c_float,
                c_float
                ]
        return func(mmin, mmax)
    def _kroupa_draw_n(self, n: int, mmin: float, mmax: float):
        func = self.lib.kroupa_draw_n

        res = (c_float * n) (*np.zeros(n))
        
        func.restype = POINTER(c_float)
        func.argtypes = [
                c_int,
                POINTER(c_float),
                c_float,
                c_float
                ]
        func(n, res, mmin, mmax)
        return np.array(res)

    def set_random_seed(self, seed: int = 314159):
        """
        Sets random seed using ``srand(unsigned seed)`` for C functions relying
        on ``rand()``.

        :param seed: seed to use for rand
        :type seed: int
        """
        func = self.lib.srand
        func.argtypes = [c_uint]

        func(seed)






ffi = FFI()
