import numpy as np

from . import distributions
from .imf import IMF


class Kroupa(IMF):
    #scale = 1/(10/13*0.5**(-1.3)+250/7*0.08**(0.7) + 20/3*(0.08**(-0.3)-0.5**(-0.3)))
    #s1 = 25
    #s2 = 2
    #I1 = (250/7*0.08**0.7)
    #I2 = (20/3*(0.08**(-0.3)-0.5**(-0.3)))
    def __init__(
            self,
            scale: float = 1/(10/13*0.5**(-1.3)+250/7*0.08**(0.7) + 20/3*(0.08**(-0.3)-0.5**(-0.3))),            
            s1: float = 25.0,
            s2: float = 2.0,
            I1 = (250/7*0.08**0.7),
            I2 = (20/3*(0.08**(-0.3)-0.5**(-0.3)))
            ):
        self.scale = scale
        self.s1 = s1
        self.s2 = s2
        self.I1 = I1
        self.I2 = I2

    def __call__(self, x: float, *args, **kwargs):
        return self.pdf(x,*args,**kwargs)

    def pdf(self,
            x: float,
            scale: float = None,
            mmin: float = None,
            mmax: float = None,
            density: bool = False,
            ):
        if scale is None:
            scale = self.scale
            if mmin is not None and mmax is not None:
                scale = 1/(self.cdf(mmax,scale=1) - self.cdf(mmin,scale=1))
            elif mmin is not None:
                scale = 1/(1-self.cdf(mmax,scale=1))
            elif mmax is not None:
                scale = 1/self.cdf(mmax,scale=1)

        if isinstance(x, np.ndarray):
            if density:
                scale = 1/(self.cdf(x.max(),scale=1) - self.cdf(x.min(),scale=1))
            ret = np.zeros(x.shape[0])
            mask = x > 0.5
            ret[mask] = x[mask]**(-2.3)
            mask = ((x > 0.08) & (x <= 0.5))
            ret[mask] = x[mask]**(-1.3) * ((0.5**(-2.3))/(0.5**(-1.3)))
            mask = x <= 0.08
            ret[mask] = x[mask]**(-0.3) * ((0.5**(-2.3))/(0.5**(-1.3)) * (0.08**(-1.3))/(0.08**(-0.3)))
            return scale*ret

        if density:
            print("ERROR: don't know how to handle density if only a single values is passed for x")

        if x > 0.5:
            return scale*x**(-2.3)
        elif x > 0.08:
            return scale*x**(-1.3) * ((0.5**(-2.3))/(0.5**(-1.3)))
        else:
            return scale*x**(-0.3) * ((0.5**(-2.3))/(0.5**(-1.3)) * (0.08**(-1.3))/(0.08**(-0.3)))

    def cdf(self, x: float, scale=None):
        if scale is None:
            scale = self.scale
        if isinstance(x, np.ndarray):
            ret = np.zeros(x.shape[0])
            mask = x > 0.5
            ret[mask] = 10/13*(0.5**(-1.3)-x[mask]**(-1.3)) + self.I1 + self.I2
            mask = ((x > 0.08) & (x <= 0.5))
            ret[mask] = 10*self.s2/3*(0.08**(-0.3)-x[mask]**(-0.3)) + self.I1
            mask = x <= 0.08
            ret[mask] = 10*self.s1/7*x[mask]**(0.7)
            return scale*ret
        
        if x > 0.5:
            return 10*scale/13*(0.5**(-1.3)-x**(-1.3)) + scale*self.I1 + scale*self.I2
        elif x > 0.08:
            return 10*scale*self.s2/3*(0.08**(-0.3)-x**(-0.3)) + scale*self.I1
        else:
            return 10*scale*self.s1/7*x**(0.7)

    def _draw_inverse_kroupa_u_from_x(self,
                                      x,
                                      scale: float = None
                                     ):
        if scale is None:
            scale = self.scale

        if x <= 0.08:
            return 10*scale*self.s1/7*x**(0.7)
        elif x <= 0.5:
            return 10*scale*self.s2/3*(0.08**(-0.3)-x**(-0.3)) + scale*self.I1
        else:
            return 10*scale/13*(0.5**(-1.3)-x**(-1.3)) + scale*self.I1 + scale*self.I2
   

    def _draw_inverse_kroupa(self,
                             x,
                             scale: float = None
                             ):
        if scale is None:
            self.scale = scale
        u_0_08 = self._draw_inverse_kroupa_u_from_x(0.08,scale=scale)
        u_0_5 = self._draw_inverse_kroupa_u_from_x(0.5,scale=scale)
        
        if isinstance(x, np.ndarray):
            y = np.zeros(x.shape[0])
            mask = x <= u_0_08
            y[mask] = (7/(10*scale*self.s1)*x[mask])**(10/7)
            mask = (x > u_0_08) & (x <= u_0_5)
            y[mask] = (0.08**(-0.3)+3*self.I1/(10*self.s2)-3/(10*scale*self.s2)*x[mask])**(-10/3)
            mask = x > u_0_5
            y[mask] = ((13/10*(self.I1+self.I2-x[mask]/scale))+0.5**(-1.3))**(-10/13)
            return y
        
        if x <= u_0_08:
            return (7/(10*scale*self.s1)*x)**(10/7)
        elif x <= u_0_5:
            return (0.08**(-0.3)+3*self.I1/(10*self.s2)-3/(10*scale*self.s2)*x)**(-10/3)
        else:
            return ((13/10*(self.I1+self.I2-x/scale))+0.5**(-1.3))**(-10/13)

    def draw(self,
             n: int = 1,
             mmin: float = 0.08,
             mmax: float = 120.0,
             scale: float = None,
             seed: int = None):
        if seed is not None:
            np.random.seed(seed)
        if scale is None:
            scale = self.scale

        u_min = self._draw_inverse_kroupa_u_from_x(mmin, scale=scale)
        u_max = self._draw_inverse_kroupa_u_from_x(mmax, scale=scale)

        u = u_min + (u_max-u_min) * np.random.random(n)

        if n == 1:
            return self._draw_inverse_kroupa(u, scale=scale)[0]
        return self._draw_inverse_kroupa(u, scale=scale)

kroupa = Kroupa()
