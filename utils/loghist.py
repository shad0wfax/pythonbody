import numpy as np

def loghist(data, bins=10, range=None, density=None, weights=None):
    hist, bins = np.histogram(data,
                              bins=bins,
                              range=range,
                              density=density,
                              weights=weights)
    logbins = np.logspace(np.log10(bins[0]),np.log10(bins[-1]),len(bins))
    hist, logbins = np.histogram(data,
                                 bins=logbins,
                                 range=range,
                                 density=density,
                                 weights=weights)
    return hist, logbins

